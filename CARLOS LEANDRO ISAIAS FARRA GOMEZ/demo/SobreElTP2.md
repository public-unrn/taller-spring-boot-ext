## TP 2 - Consultas
### Ejercicios A y B
#### A: Listar todos los cursos ; B: Listar todos los estudiantes
Cree que las interfaces CursoRepository y EstudianteRepository
que al extender de JpaRepository automaticamente se les genera un metodo
findAll() que devuelve un List con todos las entidades en la BD.

### Ejercicio C
#### Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”
En la interfaz EstudianteRepository cree el metodo @Query que 
consulta por todos los estudiantes con esas condiciones Hardcodeadas.

### Ejercicio D
#### Listar todas las inscripciones rechazadas o pendiente
En una interfaz InscripcionRepository, que extiende JpaRepository,
cree un metodo @Query que busca todas las inscripciones con estado = 'PENDIENTE'
 o 'RECHAZADO'. Con los valores hardcodeados.

### Ejercicio E

### Ejercicio F
#### Listar todos los cursos que hayan empezado después de “01/02/2020”
En la interfaz CursoRepository cree un metodo @Query que busca todos los
cursos donde fecha_inicio sea mayor a “01/02/2020”.

### Ejercicio G
#### Listar todas las inscripciones en base a un parámetro de estado
En InscripcionRepository, use una consulta parametrizada.

### Ejercicio H
#### Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa
En la interfaz InscripcionRepository, use una consulta parametrizada.

### Ejercicio I
#### Paginacion


### Nota
En la clase EscuelaService aplico todos los conceptos solicitados.