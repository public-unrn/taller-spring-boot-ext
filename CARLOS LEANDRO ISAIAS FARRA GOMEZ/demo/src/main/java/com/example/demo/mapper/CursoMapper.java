package com.example.demo.mapper;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import org.springframework.stereotype.Component;

@Component
public class CursoMapper {

    public Curso fromDTO(CursoDTO cursoDTO){
        var res = new Curso();
        res.setNombre(cursoDTO.getNombre());
        res.setDescripcion(cursoDTO.getDescripcion());
        res.setFechaInicio(cursoDTO.getFechaInicio());
        res.setFechaFin(cursoDTO.getFechaFin());

        return res;
    }

    public CursoDTO toDTO(Curso curso){
        var res = new CursoDTO();
        res.setNombre(curso.getNombre());
        res.setDescripcion(curso.getDescripcion());
        res.setFechaInicio(curso.getFechaInicio());
        res.setFechaFin(curso.getFechaFin());

        return res;
    }
}
