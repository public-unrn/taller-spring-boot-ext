package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDTO;
import com.example.demo.mapper.CursoMapper;
import com.example.demo.repository.CursoRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

@Transactional
@Service
public class CursoService {

    @Autowired
    private CursoRepository cursos;
    @Autowired
    private CursoMapper mapper;

    public void agregarCurso(CursoDTO curso){
        cursos.save(mapper.fromDTO(curso));
    }

    public CursoDTO buscarCurso(Long id){
        return mapper.toDTO(cursos.findById(id).orElseThrow());
    }

    public List<CursoDTO> listarCursos() {
        return Collections.unmodifiableList(cursos.findAll().stream().map(mapper::toDTO).toList());
    }

    public List<CursoDTO> cursosPorFechaInicio(LocalDate fechaInicio){
        var res =  cursos.buscarPorFecha(fechaInicio);
        System.out.println("TAMAÑO DE LA LISTA: " + res.size());
        return res.stream().map(mapper::toDTO).toList();
    }
}
