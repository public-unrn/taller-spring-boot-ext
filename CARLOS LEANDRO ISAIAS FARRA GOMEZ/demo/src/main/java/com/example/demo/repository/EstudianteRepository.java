package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.awt.print.Pageable;
import java.util.List;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante, Long>{

    // Punto C
    @Query("SELECT e FROM Estudiante e WHERE e.apellido = 'Romero' AND e.dni > 20000000")
    public List<Estudiante> buscarRomeroDniMayorA20M();

    public List<Estudiante> findByApellidoAndDniGreaterThan(String apellido, Integer dni);
}
