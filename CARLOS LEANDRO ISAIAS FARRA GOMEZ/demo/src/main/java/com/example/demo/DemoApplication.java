package com.example.demo;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.service.CursoService;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.time.Month;

@SpringBootApplication
public class DemoApplication {

	@Autowired
	private Tp2Consultas ejercicios;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {
			System.out.println("A. Listar todos los cursos");
			System.out.println(ejercicios.listarCursos());

			System.out.println("B. Listar todos los estudiantes");
			System.out.println(ejercicios.listarEstudiantes());

			System.out.println("C. Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”");
			System.out.println(ejercicios.listarEstudiantesPorApellidoYNac("Romero", 20_000_000));

			System.out.println("D. Listar todas las inscripciones rechazadas o pendiente");
			System.out.println(ejercicios.listarInscripcionesPendientesORechazadas());

			System.out.println("F. Listar todos los cursos que hayan empezado después de “01/02/2020”");
			System.out.println(ejercicios.listarCursosPostFecha(LocalDate.of(2020, Month.FEBRUARY, 1)));

			System.out.println("G. Listar todas las inscripciones en base a un parámetro de estado");
			System.out.println(ejercicios.listarInscripcionesPorEstadoParametrizado(EstadoInscripcion.ACEPTADA));

			System.out.println("H. Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa");
			System.out.println(ejercicios.listarInscripcionesPorEstadoNativo(EstadoInscripcion.ACEPTADA));

			System.out.println("I. Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI");
			System.out.println("a. pagina 1, tamaño 5");
			System.out.println(ejercicios.listarEstudiantesPorPagina(1, 5));
			System.out.println("b. pagina 0, tamaño 2");
			System.out.println(ejercicios.listarEstudiantesPorPagina(0, 2));


			System.out.println("Servicio y Transaccion");
			ejercicios.inscribirEstudiante(7L,1L);

		};
	}

}


