package com.example.demo.mapper;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class EstudianteMapper {

    public Estudiante fromDTO(EstudianteDTO e){
        var res = new Estudiante();
        res.setNombre(e.getNombre());
        res.setApellido(e.getApellido());
        res.setEmail(e.getEmail());
        res.setDni(e.getDni());
        res.setFechaNacimiento(e.getFechaNacimiento());

        return res;
    }

    public EstudianteDTO toDTO(Estudiante e){
        var res = new EstudianteDTO();
        res.setNombre(e.getNombre());
        res.setApellido(e.getApellido());
        res.setEmail(e.getEmail());
        res.setDni(e.getDni());
        res.setFechaNacimiento(e.getFechaNacimiento());

        return res;
    }

    public void updateFromDTO(Estudiante entidad, EstudianteDTO valoresNuevos){
        entidad.setNombre(intercambiarSiExiste(entidad.getNombre(), valoresNuevos.getNombre()));
        entidad.setApellido(intercambiarSiExiste(entidad.getApellido(), valoresNuevos.getApellido()));
        entidad.setEmail(intercambiarSiExiste(entidad.getEmail(), valoresNuevos.getEmail()));
        entidad.setDni(intercambiarSiExiste(entidad.getDni(), valoresNuevos.getDni()));
        entidad.setFechaNacimiento(intercambiarSiExiste(entidad.getFechaNacimiento(),valoresNuevos.getFechaNacimiento()));
    }

    private static <T> T intercambiarSiExiste(T valorViejo, T valorNuevo){
        return (Objects.isNull(valorNuevo)) ? valorViejo : valorNuevo;
    }
}
