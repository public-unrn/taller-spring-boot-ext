package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EstudianteDTO {
    private String nombre;
    private String apellido;
    private String email;
    private Integer dni;
    private LocalDate fechaNacimiento;
}
