package com.example.demo.service;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;

@Transactional
@Service
public class InscripcionService {

    @Autowired
    private InscripcionRepository inscripciones;
    @Autowired
    private EstudianteRepository estudiantes;
    @Autowired
    private CursoRepository cursos;

    public List<Inscripcion> listarTodas(){
        return inscripciones.findAll();
    }

    public void inscribir(InscripcionDTO inscripcion){
        var estudiante = estudiantes.findById(inscripcion.getIdEstudiante()).orElseThrow(NoSuchElementException::new);
        var curso = cursos.findById(inscripcion.getIdCurso()).orElseThrow(NoSuchElementException::new);

        if(!estudiante.isMayorEdad())
            throw new RuntimeException("Debe ser mayor de Edad");

        inscripciones.save(new Inscripcion(null, LocalDate.now(), EstadoInscripcion.PENDIENTE, curso, estudiante));
    }

    public List<Inscripcion> listarInscripcionesPorEstado(EstadoInscripcion e1, EstadoInscripcion e2){
        return inscripciones.findByEstadoOrEstado(e1, e2);
    }

    public List<Inscripcion> listarPorEstadoParametrizado(EstadoInscripcion estado){
        return inscripciones.buscarPorEstadoParametrizado(estado);
    }

    public List<Inscripcion> listarPorEstadoNativo(EstadoInscripcion estado){
        return inscripciones.buscarPorEstadoNativo(estado.toString());
    }



}
