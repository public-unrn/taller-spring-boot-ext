package com.example.demo;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.CursoService;
import com.example.demo.service.EstudianteService;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class Tp2Consultas {

    @Autowired
    private CursoService cursos;
    @Autowired
    private EstudianteService estudiantes;
    @Autowired
    private InscripcionService inscripciones;

    public List<CursoDTO> listarCursos(){
        return cursos.listarCursos();
    }

    public List<EstudianteDTO> listarEstudiantes(){
        return estudiantes.listarEstudiantesOrdenadoPorDni();
    }

    public List<Estudiante> listarEstudiantesPorApellidoYNac(String apellido, Integer dni) {
        return (estudiantes.listarEstudiantesPorApellidoYNac(apellido, dni));
    }

    public List<Inscripcion> listarInscripcionesPendientesORechazadas(){
        return inscripciones.listarInscripcionesPorEstado(EstadoInscripcion.PENDIENTE, EstadoInscripcion.RECHAZADA);
    }

    public List<CursoDTO> listarCursosPostFecha(LocalDate fecha){
        return cursos.cursosPorFechaInicio(fecha);
    }

    public List<Inscripcion> listarInscripcionesPorEstadoParametrizado(EstadoInscripcion estado){
        return inscripciones.listarPorEstadoParametrizado(estado);
    }

    public List<Inscripcion> listarInscripcionesPorEstadoNativo(EstadoInscripcion estado){
        return inscripciones.listarPorEstadoNativo(estado);
    }

    public List<EstudianteDTO> listarEstudiantesPorPagina(Integer numeroPagina, Integer tamanioPagina){
        return estudiantes.listarEstudiantesOrdenadoPorDni(numeroPagina, tamanioPagina);
    }

    public void inscribirEstudiante(Long idEstudiante, Long idCurso){
        inscripciones.inscribir(new InscripcionDTO(idCurso,idEstudiante));
    }

}
