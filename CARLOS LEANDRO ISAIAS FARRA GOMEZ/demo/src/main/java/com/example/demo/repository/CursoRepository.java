package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {

//2020-02-01
    @Query(value = "SELECT c FROM Curso c WHERE c.fechaInicio > :fecha")
    List<Curso> buscarPorFecha(@Param("fecha")LocalDate fecha);
}
