package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EstudiantePagingRepository extends PagingAndSortingRepository<Estudiante, Long> {
    public List<Estudiante> findByApellidoAndDniGreaterThan(String apellido, Integer dni);

    public Optional<Estudiante> findById(Long id);
}
