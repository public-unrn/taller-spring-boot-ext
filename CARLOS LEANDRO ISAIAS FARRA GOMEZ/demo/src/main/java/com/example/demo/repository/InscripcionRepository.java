package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Inscripcion;
import org.hibernate.annotations.NamedNativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {


    @Query("SELECT i FROM Inscripcion i WHERE i.estado = 'RECHAZADA' OR i.estado = 'PENDIENTE'")
    public List<Inscripcion> buscarPorEstado();
    public List<Inscripcion> findByEstadoOrEstado(EstadoInscripcion estado1, EstadoInscripcion estado2);

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :estado")
    public List<Inscripcion> buscarPorEstadoParametrizado(@Param("estado") EstadoInscripcion estado);

    @Query(nativeQuery = true, value = "SELECT * FROM Inscripcion i WHERE i.estado = :estado")
    public List<Inscripcion> buscarPorEstadoNativo(@Param("estado") String estado);
}
