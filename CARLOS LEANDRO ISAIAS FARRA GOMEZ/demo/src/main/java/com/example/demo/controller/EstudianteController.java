package com.example.demo.controller;

import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudiantes;

    @PostMapping
    public void agregarEstudiante(@RequestBody EstudianteDTO estudiante){
        estudiantes.agregarEstudiante(estudiante);
    }

    @GetMapping
    public List<EstudianteDTO> listarEstudiantes(){
        return estudiantes.listarEstudiantesOrdenadoPorDni();
    }

    @GetMapping("/{id}")
    public EstudianteDTO buscarEstudiante(@PathVariable Long id){
        return estudiantes.buscarEstudiante(id);
    }

    @PutMapping("/{id}")
    public void actualizarEstudiante(@PathVariable Long id, @RequestBody EstudianteDTO estudiante){
        estudiantes.actualizarEstudiante(id, estudiante);
    }

    @DeleteMapping("/{id}")
    public void borrarEstudiante(@PathVariable Long id){
        estudiantes.borrarEstudiante(id);
    }
}
