package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "estudiante")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private String apellido;

    private String email;

    private Integer dni;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    public Boolean isMayorEdad(){
        var hoy = LocalDate.now();
        var periodoAnhos = Period.between(fechaNacimiento, hoy).getYears();
        return periodoAnhos >= 18;
    }
}
