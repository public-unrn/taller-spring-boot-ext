package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.mapper.EstudianteMapper;
import com.example.demo.repository.EstudianteRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Transactional
@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudiantes;
    @Autowired
    private EstudianteMapper mapper;

    public void agregarEstudiante(EstudianteDTO e){
        estudiantes.save(mapper.fromDTO(e));
    }

    public List<EstudianteDTO> listarEstudiantesOrdenadoPorDni(){
        return Collections.unmodifiableList(estudiantes.findAll().stream().map(mapper::toDTO).toList());
    }

    public EstudianteDTO buscarEstudiante(Long id){
        return mapper.toDTO(estudiantes.findById(id).get());
    }

    public void actualizarEstudiante(Long id, EstudianteDTO e){
        var estudiante = estudiantes.findById(id);
        if (estudiante.isPresent()){
            var updateEstudiante = estudiante.get();
            mapper.updateFromDTO(updateEstudiante, e);
            estudiantes.save(updateEstudiante);
        }
    }

    public void borrarEstudiante(Long id){
        estudiantes.deleteById(id);
    }

    public List<EstudianteDTO> listarEstudiantesOrdenadoPorDni(Integer numeroPagina, Integer tamPagina) {
        var pagina = PageRequest.of(numeroPagina,tamPagina, Sort.by("dni").ascending());
        return estudiantes.findAll(pagina).getContent().stream().map(mapper::toDTO).toList();
    }

    public List<Estudiante> listarEstudiantesPorApellidoYNac(String apellido, Integer dni) {
        return Collections.unmodifiableList(estudiantes.findByApellidoAndDniGreaterThan(apellido,dni));
    }
}
