package ar.unrn.edu.repository;

import ar.unrn.edu.domain.Estudiante;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {
    @Query("select e from Estudiante e")
    List<Estudiante> findAllEstudiantes();

    @Query("select e from Estudiante e where e.dni > :dniMayorA and e.apellido = :apellido")
    List<Estudiante> findByApellidoAndDniMayorA(@Param("dniMayorA") int dniMayorA, @Param("apellido") String apellido);

}
