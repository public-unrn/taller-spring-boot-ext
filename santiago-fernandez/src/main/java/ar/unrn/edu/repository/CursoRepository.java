package ar.unrn.edu.repository;

import ar.unrn.edu.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CursoRepository extends JpaRepository<Curso, Long> {


    @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > :fecha")
    List<Curso> findAllCursosAfterDate(@Param("fecha") Date fecha);

    List<Curso> findAll();

}
