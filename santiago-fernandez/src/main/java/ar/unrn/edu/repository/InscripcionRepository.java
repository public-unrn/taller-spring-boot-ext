package ar.unrn.edu.repository;

import ar.unrn.edu.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long>{
    @Query("SELECT i FROM Inscripcion i where i.estado = 'Pendiente' OR i.estado = 'Rechazada'")
    List<Inscripcion> allByEstadoPendienteOrStateRechazada();
    List<Inscripcion> findAllByEstadoOrEstado(String estado, String estado2);
    @Query(value = "SELECT * FROM Inscripcion i where i.estado = 'ACCEPTED'",nativeQuery = true)
    List<Inscripcion> findAllByEstadoNative(String estado);
    List<Inscripcion> findAllByEstado(String estado);


}
