package ar.unrn.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TallerSpringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TallerSpringbootApplication.class, args);
	}

}
