package ar.unrn.edu.controllers;


import ar.unrn.edu.dto.CursoDTO;
import ar.unrn.edu.dto.InscripcionDTO;
import ar.unrn.edu.services.InscripcionService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("inscripciones")
public class InscripcionController {

    @Autowired
    InscripcionService inscripcionService;

    @PostMapping("/crear")
    @Operation(summary = "Agregar una inscripcion")
    public ResponseEntity<?> create(@RequestBody InscripcionDTO inscripcion) {
        this.inscripcionService.create(inscripcion.getCurso().getId(),inscripcion.getEstudiante().getId(),inscripcion.getFechaDeInscripcion(),inscripcion.getEstado());
        return ResponseEntity.status(OK).body("La inscripción se añadió con éxito!");
    }

}
