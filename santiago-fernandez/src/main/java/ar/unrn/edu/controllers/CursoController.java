package ar.unrn.edu.controllers;

import ar.unrn.edu.dto.CursoDTO;
import ar.unrn.edu.services.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.v3.oas.annotations.Operation;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("cursos")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping("/crear")
    @Operation(summary = "Agregar un curso")
    public ResponseEntity<?> create(@RequestBody CursoDTO curso) {
        this.cursoService.create(curso.getDescripcion(),curso.getNombre(),curso.getFechaDeInicio(),curso.getFechaDeFin());
        return ResponseEntity.status(OK).body("El curso se añadió con éxito!");
    }

}
