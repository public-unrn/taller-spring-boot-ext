package ar.unrn.edu.controllers;

import ar.unrn.edu.dto.EstudianteDTO;
import ar.unrn.edu.services.EstudianteService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("estudiantes")
public class EstudianteController {

    @Autowired
    EstudianteService estudianteService;

    @PostMapping("/crear")
    @Operation(summary = "Agregar un estudiante")
    public ResponseEntity<?> create(@RequestBody EstudianteDTO estudiante) {
        this.estudianteService.create(estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getEdad(),estudiante.getDni(),estudiante.getFechaDeNacimiento());
        return ResponseEntity.status(OK).body("El estudiante se añadió con éxito!");
    }

    @PutMapping("/actualizar/{id}")
    @Operation(summary = "Actualizar un estudiante")
    public ResponseEntity<?> update(@RequestBody EstudianteDTO estudiante) {
        this.estudianteService.update(estudiante.getId(),estudiante.getNombre(),estudiante.getApellido(),estudiante.getEmail(),estudiante.getEdad(),estudiante.getEdad(),estudiante.getFechaDeNacimiento());
        return ResponseEntity.status(OK).body("El estudiante se actualizó con éxito!");
    }

    @DeleteMapping("/eliminar/{id}")
    @Operation(summary = "Eliminar un estudiante")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        this.estudianteService.delete(id);
        return ResponseEntity.status(OK).body("El estudiante se eliminó con éxito!");
    }

    @GetMapping("/listar")
    @Operation(summary = "Obtener todos los estudiantes")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.status(OK).body(this.estudianteService.findAll());
    }

    @GetMapping("/listar/{id}")
    @Operation(summary = "Obtener un estudainte")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.status(OK).body(this.estudianteService.findById(id));
    }


}
