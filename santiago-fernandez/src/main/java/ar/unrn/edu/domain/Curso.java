package ar.unrn.edu.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "cursos")
@Getter
@NoArgsConstructor
public class Curso {
    @Id
    @GeneratedValue
    private Long id;
    private String descripcion;
    @Column(name = "fecha_de_inicio")
    private LocalDate fechaDeInicio;
    @Column(name = "fecha_de_fin")
    private LocalDate fechaDeFin;
    private String nombre;

    public Curso(String descripcion, LocalDate fechaDeInicio, LocalDate fechaDeFin, String nombre) {
        this.descripcion = descripcion;
        this.fechaDeInicio = fechaDeInicio;
        this.fechaDeFin = fechaDeFin;
        this.nombre = nombre;
    }

}
