package ar.unrn.edu.domain;

import jakarta.persistence.*;
import lombok.NoArgsConstructor;

import java.time.LocalDate;


@Entity
@Table(name = "inscripciones")
@NoArgsConstructor
public class Inscripcion {

    @Id
    @GeneratedValue
    private Long id;
    private String estado;
    @ManyToOne
    @JoinColumn(name= "curso_id")
    private Curso curso;

    @Column(name = "fecha_de_inscripcion")
    private LocalDate fechaDeInscripcion;
    @ManyToOne
    @JoinColumn(name = "estudiante_id")
    private Estudiante estudiante;

    public Inscripcion(String estado, Curso curso, LocalDate fechaDeInscripcion, Estudiante estudiante) {
        this.estado = estado;
        this.curso = curso;
        this.fechaDeInscripcion = fechaDeInscripcion;
        this.estudiante = estudiante;
    }

}
