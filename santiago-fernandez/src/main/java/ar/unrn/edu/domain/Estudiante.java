package ar.unrn.edu.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "estudiantes")
@Getter
@Setter
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int dni;
    private String email;
    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;
    private String apellido;
    private int edad;
    private String nombre;

    public Estudiante(int dni, String email, LocalDate fechaDeNacimiento, String apellido, int edad, String nombre) {
        this.dni = dni;
        this.email = email;
        this.fechaDeNacimiento = fechaDeNacimiento;
        this.apellido = apellido;
        this.edad = edad;
        this.nombre = nombre;
    }

    public boolean sosMayorDeEdad(){
        long age = ChronoUnit.YEARS.between(this.fechaDeNacimiento, LocalDate.now());
        if(age > 18){
            return true;
        }
        return false;
    }

}
