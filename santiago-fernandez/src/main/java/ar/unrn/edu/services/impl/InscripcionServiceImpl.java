package ar.unrn.edu.services.impl;

import ar.unrn.edu.domain.Curso;
import ar.unrn.edu.domain.Estudiante;
import ar.unrn.edu.domain.Inscripcion;
import ar.unrn.edu.dto.InscripcionDTO;
import ar.unrn.edu.repository.CursoRepository;
import ar.unrn.edu.repository.EstudianteRepository;
import ar.unrn.edu.repository.InscripcionRepository;
import ar.unrn.edu.services.InscripcionService;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class InscripcionServiceImpl implements InscripcionService {

    @Autowired
    InscripcionRepository inscripcionRepository;
    @Autowired
    CursoRepository cursoRepository;
    @Autowired
    EstudianteRepository estudianteRepository;

    @Override
    public void create(@NotNull @Positive Long idCurso, @NotNull @Positive Long idEstudiante, @NotNull LocalDate fecha, @NotNull String status) {
        Curso curso= cursoRepository.findById(idCurso).orElseThrow(() -> new RuntimeException("La id del curso no es valida"));
        Estudiante estudiante= estudianteRepository.findById(idEstudiante).orElseThrow(() -> new RuntimeException("el id del estudiante no es valido"));
        if(!estudiante.sosMayorDeEdad())
            throw new RuntimeException("El estudiante es menor de edad");
        this.inscripcionRepository.save(new Inscripcion(status, curso, fecha, estudiante));
    }


}
