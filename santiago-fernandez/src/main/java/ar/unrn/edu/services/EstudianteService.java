package ar.unrn.edu.services;

import ar.unrn.edu.dto.EstudianteDTO;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.time.LocalDate;
import java.util.List;

public interface EstudianteService {

    void create(String nombre, String apellido,  String email, int edad,  int dni, LocalDate fechaNacimiento);

    List<EstudianteDTO> findAll();

    EstudianteDTO findById(Long id);

    void update(Long id,String nombre,String apellido,String email,int edad,@Positive int dni,LocalDate fechaNacimiento);

    void delete(Long id);

}
