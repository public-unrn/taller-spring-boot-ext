package ar.unrn.edu.services.impl;

import ar.unrn.edu.domain.Estudiante;
import ar.unrn.edu.dto.EstudianteDTO;
import ar.unrn.edu.repository.EstudianteRepository;
import ar.unrn.edu.services.EstudianteService;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EstudianteServiceImpl implements EstudianteService {
    EstudianteRepository estudianteRepository;

    @Override
    public void create(@NotNull @NotEmpty String nombre, @NotNull @NotEmpty String apellido, @NotNull @NotEmpty String email, @NotNull @Positive int edad, @NotNull @Positive int dni, @NotNull LocalDate fechaNacimiento) {
        this.estudianteRepository.save(new Estudiante(dni,email,fechaNacimiento, apellido, edad, nombre));
    }
    @Override
    public List<EstudianteDTO> findAll() {
        return this.parseListToEstudiantesDTO(this.estudianteRepository.findAll());
    }
    @Override
    public EstudianteDTO findById(@NotNull @Positive Long id) {
        Estudiante estudiante= estudianteRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("La id del estudiante no es valida"));
        return this.parseToEstudianteDTO(estudiante);
    }


    @Override
    public void update(@NotNull @Positive Long id,@NotNull @NotEmpty String nombre, @NotNull @NotEmpty String apellido, @NotNull @NotEmpty String email, @NotNull @Positive int edad, @NotNull @Positive int dni, @NotNull LocalDate fechaNacimiento) {
        Estudiante estudiante= estudianteRepository.getReferenceById(id);
        estudiante.setApellido(estudiante.getApellido());
        estudiante.setNombre(estudiante.getNombre());
        estudiante.setDni(estudiante.getDni());
        estudiante.setEmail(estudiante.getEmail());
        estudiante.setFechaDeNacimiento(estudiante.getFechaDeNacimiento());
        estudianteRepository.save(estudiante);
    }

    @Override
    public void delete(@NotNull @Positive Long id) {
        estudianteRepository.findById(id).orElseThrow(() -> new RuntimeException("La id del estudiante no es valida"));
        estudianteRepository.deleteById(id);
    }

    private List<EstudianteDTO> parseListToEstudiantesDTO(List<Estudiante> estudiantes){
        List<EstudianteDTO> estudianteDTOS = new ArrayList<>();
        for(Estudiante estudiante: estudiantes){
            estudianteDTOS.add(this.parseToEstudianteDTO(estudiante));
        }
        return estudianteDTOS;
    }

    private EstudianteDTO parseToEstudianteDTO(Estudiante estudiante){
        return new EstudianteDTO(
                estudiante.getId(),
                estudiante.getDni(),
                estudiante.getEmail(),
                estudiante.getFechaDeNacimiento(),
                estudiante.getApellido(),
                estudiante.getEdad(),
                estudiante.getNombre()
        );
    }



}
