package ar.unrn.edu.services;

import ar.unrn.edu.dto.InscripcionDTO;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

import java.time.LocalDate;

public interface InscripcionService {

    void create(Long idCurso, Long idEstudiante,LocalDate fecha,String status);

}
