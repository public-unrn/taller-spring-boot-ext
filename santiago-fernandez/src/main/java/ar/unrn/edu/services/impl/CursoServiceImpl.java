package ar.unrn.edu.services.impl;


import ar.unrn.edu.domain.Curso;
import ar.unrn.edu.dto.CursoDTO;
import ar.unrn.edu.repository.CursoRepository;
import ar.unrn.edu.services.CursoService;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CursoServiceImpl implements CursoService {

    @Autowired
    CursoRepository cursoRepository;

    public void create(@NotNull @NotEmpty String nombre, @NotNull @NotEmpty String descripcion, @NotNull LocalDate fechaInicio, @NotNull LocalDate fechaFin){
        this.cursoRepository.save(new Curso(descripcion,fechaInicio,fechaFin,nombre));
    }



}
