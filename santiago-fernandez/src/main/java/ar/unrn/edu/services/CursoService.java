package ar.unrn.edu.services;

import ar.unrn.edu.dto.CursoDTO;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;

public interface CursoService {


    void create(String nombre, String descripcion, LocalDate fechaInicio, LocalDate fechaFin);

}
