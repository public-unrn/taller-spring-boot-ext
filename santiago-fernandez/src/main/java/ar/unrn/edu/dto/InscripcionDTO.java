package ar.unrn.edu.dto;

import ar.unrn.edu.domain.Curso;
import ar.unrn.edu.domain.Estudiante;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

import java.time.LocalDate;

@Data
public class InscripcionDTO {
    private Long id;
    private String estado;
    private CursoDTO curso;
    private LocalDate fechaDeInscripcion;
    private EstudianteDTO estudiante;

}
