package ar.unrn.edu.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class EstudianteDTO {
    private Long id;
    private int dni;
    private String email;
    private LocalDate fechaDeNacimiento;
    private String apellido;
    private int edad;
    private String nombre;

    public EstudianteDTO(Long id, int dni, String email, LocalDate fechaDeNacimiento, String apellido, int edad, String nombre) {
    }
}
