package ar.unrn.edu.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class CursoDTO {
    private Long id;
    private String descripcion;
    private LocalDate fechaDeInicio;
    private LocalDate fechaDeFin;
    private String nombre;

}
