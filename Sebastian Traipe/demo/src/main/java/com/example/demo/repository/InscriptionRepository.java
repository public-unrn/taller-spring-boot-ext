package com.example.demo.repository;

import com.example.demo.domain.Inscription;
import com.example.demo.domain.Status;
import com.example.demo.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface InscriptionRepository extends JpaRepository<Inscription, Long> {

    //  Listar todas las inscripciones rechazado y pendiente
    @Query(value = "SELECT * FROM Inscription WHERE status IN ('RECHAZADO', 'PENDIENTE')", nativeQuery = true)
    List<Inscription> findRejectedOrPendingInscriptionsNative();

    //  Listar todas las inscripciones mediante estados
    List<Inscription> findByStatusIn(List<Student> status);

    //  Listar todas las inscripciones en base a un parámetro de estado
    List<Inscription> findByStatus(Status status);

    //  Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa:
    @Query(value = "SELECT * FROM Inscription WHERE status = :status", nativeQuery = true)
    List<Inscription> findByStatusNative(String status);
}
