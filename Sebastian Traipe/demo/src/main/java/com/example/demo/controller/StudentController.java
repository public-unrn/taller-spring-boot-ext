package com.example.demo.controller;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentDTO;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping
    public StudentDTO save(StudentDTO studentDTO) {
        return this.studentService.saveStudent(studentDTO);
    }

    @GetMapping
    public List<StudentDTO> findAll() {
        return this.studentService.findAll();
    }

    @GetMapping("/{id}")
    public StudentDTO find(@PathVariable Long id) {
        return this.studentService.find(id);
    }

    @PutMapping("{id}")
    public StudentDTO studentDTO(@PathVariable Long id, @RequestBody StudentDTO studentDTO) {
        return this.studentService.update(id, studentDTO);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        this.studentService.delete(id);
    }
}
