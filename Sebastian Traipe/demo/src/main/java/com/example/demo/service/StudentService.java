package com.example.demo.service;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentDTO;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Transactional
    public StudentDTO saveStudent(StudentDTO studentDTO) {
        Student student = new Student(
                null,
                studentDTO.getFirstName(),
                studentDTO.getLastName(),
                studentDTO.getEmail(),
                studentDTO.getDni(),
                studentDTO.getBirthDate()
        );
        this.studentRepository.save(student);
        return studentDTO;
    }

    @Transactional
    public List<StudentDTO> findAll() {
        return this.studentRepository.findAll()
                .stream().map(s -> new StudentDTO(s.getFirstName(), s.getLastName(),
                        s.getEmail(), s.getDni(), s.getBirthDate()))
                .collect(Collectors.toList());
    }

    @Transactional
    public StudentDTO update(Long id, StudentDTO studentDTO) {
        Student student = new Student(id, studentDTO.getFirstName(), studentDTO.getLastName(),
                studentDTO.getEmail(), studentDTO.getDni(), studentDTO.getBirthDate());

        this.studentRepository.save(student);
        return studentDTO;
    }

    @Transactional
    public StudentDTO find(Long id) {
        Student student = this.studentRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("El id del estudiante no es valido..."));

        return new StudentDTO(student.getFirstName(), student.getLastName(), student.getEmail(),
                student.getDni(), student.getBirthDate());
    }

    @Transactional
    public void delete(Long id) {
        this.studentRepository.deleteById(id);
    }
}
