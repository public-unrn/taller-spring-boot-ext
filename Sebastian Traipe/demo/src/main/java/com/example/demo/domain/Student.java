package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.Period;

@Entity
@Table(name = "students", uniqueConstraints = @UniqueConstraint(columnNames = "dni"))
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;
    private String email;
    private int dni;

    @Column(name = "birth_date")
    private LocalDate birthDate;

    public boolean esMayorEdad() {
        Period age = Period.between(this.birthDate, LocalDate.now());
        return age.getYears() >= 18;
    }
}
