package com.example.demo.controller;

import com.example.demo.dto.InscriptionDTO;
import com.example.demo.service.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/inscription")
public class InscriptionController {

    @Autowired
    private InscriptionService inscriptionService;

    @PostMapping
    public void save(@RequestBody InscriptionDTO inscriptionDTO) {
        this.inscriptionService.saveInscription(inscriptionDTO.getStudent(), inscriptionDTO.getCourse(),
                inscriptionDTO.getEnrollmentDate());
    }
}
