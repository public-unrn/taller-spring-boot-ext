package com.example.demo.service;

import com.example.demo.domain.Course;
import com.example.demo.domain.Inscription;
import com.example.demo.domain.Status;
import com.example.demo.domain.Student;
import com.example.demo.repository.CourseRepository;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@Validated
public class InscriptionService {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private InscriptionRepository inscriptionRepository;

    @Transactional
    public void saveInscription(@NotNull @Positive Long idStudent, @NotNull @Positive Long idCourse,
                            LocalDate enrollmentDate) {

        Student student = this.studentRepository
                .findById(idStudent)
                .orElseThrow(() -> new RuntimeException("El id del estudiante no es valido..."));

        if (!student.esMayorEdad()) {
            throw new RuntimeException("El estudiante es menor de edad...");
        }

        Course course = this.courseRepository
                .findById(idCourse)
                .orElseThrow(() -> new RuntimeException("El id del curso no es valido..."));

        Inscription inscription = new Inscription(null, enrollmentDate, Status.PENDIENTE, course, student);

        this.inscriptionRepository.save(inscription);
    }
}
