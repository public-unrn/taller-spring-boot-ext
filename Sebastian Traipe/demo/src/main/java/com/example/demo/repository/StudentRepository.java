package com.example.demo.repository;

import com.example.demo.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

    // Listar todos los estudiantes mediante consulta nativa
    @Query(value = "SELECT * FROM Student", nativeQuery = true)
    List<Student> findAllStudentsNative();

    // Listar todos los estudiantes que tengan un dni mayor a 'tanto' y su apellido sea 'tanto'
    @Query(value = "SELECT * FROM Student WHERE CAST(dni AS INT) > :dni AND last_name = :lastName", nativeQuery = true)
    List<Student> findByDniGreaterThanAndLastNameEqualsNative(@Param("dni") String dni, @Param("lastName") String lastName);

    // Listar todos los estudiantes
    List<Student> findAll();

    // Listar todos los estudiantes que tengan un dni mayor a 'tanto' y su apellido sea 'tanto'
    List<Student> findByDniGreaterThanAndLastNameEquals(int dni, String lastName);

    // Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI:
    Page<Student> findAllByOrderByDniAsc(Pageable pageable);
}
