package com.example.demo.service;

import com.example.demo.domain.Course;
import com.example.demo.dto.CourseDTO;
import com.example.demo.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Transactional
    public CourseDTO saveCourse(CourseDTO courseDTO) {
        Course course = new Course(
                null,
                courseDTO.getName(),
                courseDTO.getDescription(),
                courseDTO.getStartDate(),
                courseDTO.getEndDate()
        );
        this.courseRepository.save(course);
        return courseDTO;
    }
}
