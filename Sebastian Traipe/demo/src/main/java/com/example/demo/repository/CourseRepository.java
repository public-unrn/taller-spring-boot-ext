package com.example.demo.repository;

import com.example.demo.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.util.List;

public interface CourseRepository extends JpaRepository<Course, Long> {

    // Listar todos los cursos mediante consulta nativa
    @Query(value = "SELECT * FROM Course", nativeQuery = true)
    List<Course> findAllCoursesNative();

    //  Listar todos los cursos que hayan empezado después de “??/??/????”, mediante consulta nativa
    @Query(value = "SELECT * FROM Course WHERE start_date > :date", nativeQuery = true)
    List<Course> findByStartDateAfterNative(@Param("date") LocalDate date);

    //  Listar todos los cursos mediante consulta derivada
    List<Course> findAll();

    //  Listar todos los cursos que hayan empezado después de “01/02/2020”
    List<Course> findByStartDateAfter(LocalDate date);
}
