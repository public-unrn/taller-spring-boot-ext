package com.example.demo.repository;

import java.util.List;

import org.springdoc.core.converters.models.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import com.example.demo.domain.Estudiante;

public interface EstudianteRepository extends JpaRepository<Estudiante,Long>{

    //consulta jpql
    @Query(value = "SELECT e FROM Estudiante e WHERE e.dni >= 20000000")
    List<Estudiante> findAllEstudianteMayorDni();
    
    //consulta derivada no la pude hacer
    List<Estudiante> findByDniContaining(int dni);
}
