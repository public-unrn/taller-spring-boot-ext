package com.example.demo.dto;


public enum EstadoDto {
    ACEPTADO,
    RECHAZADO,
    PENDIENTE;
}