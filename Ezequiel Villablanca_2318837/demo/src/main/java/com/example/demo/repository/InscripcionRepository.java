package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;

public interface  InscripcionRepository extends JpaRepository<Inscripcion,Long>,InscripcionRepositoryCustom{
    
    @Query(value = "SELECT i FROM Inscripcion i WHERE i.estado LIKE '%RECHAZADO%'")
    List<Inscripcion> findAllEstadoRechazado();


    List<Inscripcion> findAllByEstadoLike(Estado estado);
    List<Inscripcion> findAllInscripcionNativa(Estado estado);
    //para saber si es mayor a algo en derivada es "greater than"
    Optional<Inscripcion> findByCurso_nombre(Estado estado);

    

}
