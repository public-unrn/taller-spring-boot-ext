package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.CursoDto;
import com.example.demo.dto.EstudianteDto;
import com.example.demo.dto.InscripcionDto;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;

import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

@Service
@Validated
public class InscripcionService {
    
    @Autowired
    InscripcionRepository inscripcionRepository;
    @Autowired
    EstudianteRepository estudianteRepository;
    @Autowired
    CursoRepository cursoRepository;

    @Transactional
    public InscripcionDto persistirInscripcion(InscripcionDto inscripcionDto){

        Estudiante estudiante=new Estudiante(null,inscripcionDto.getEstudiante().getNombre(),inscripcionDto.getEstudiante().getApellido(),inscripcionDto.getEstudiante().getEmail(),inscripcionDto.getEstudiante().getDni(),inscripcionDto.getEstudiante().getFechaNacimiento());
        Curso curso= new Curso(null,inscripcionDto.getCurso().getNombre(),inscripcionDto.getCurso().getDescripcion(),inscripcionDto.getCurso().getFechaInicio(),inscripcionDto.getCurso().getFechaFin());
        Inscripcion inscripcion=new Inscripcion(null,inscripcionDto.getFechaInscripcion(),inscripcionDto.getEstado(),estudiante,curso);
        estudianteRepository.save(estudiante);
        cursoRepository.save(curso);

        inscripcionRepository.save(inscripcion);
        return inscripcionDto;
    }
    public List<InscripcionDto> getInscripciones(){

        List<Inscripcion> inscripciones = inscripcionRepository.findAll();
        
        List<InscripcionDto> inscripcionesDto= inscripciones.stream().map(i-> new InscripcionDto(i.getId(), i.getFechaInscripcion(), i.getEstado(),
        new EstudianteDto(i.getEstudiante().getId(),i.getEstudiante().getNombre(),i.getEstudiante().getApellido(),i.getEstudiante().getEmail(),i.getEstudiante().getDni(),i.getEstudiante().
        getFechaNacimiento()),
        new CursoDto(i.getCurso().getId(),i.getCurso().getNombre(),i.getCurso().getDescripcion(),i.getCurso().getFechaInicio(),i.getCurso().getFechaFin()))).collect(Collectors.toList());
        return inscripcionesDto;
    }
    @Transactional
   public void registrarInscripcionModuloTres(@NotNull @Positive Long idCurso,@NotNull @Positive Long idEstudiante){

        Estudiante estudiante = estudianteRepository.findById(idEstudiante).orElseThrow(()->new RuntimeException("El id  del estudiante es invalido") );
        Curso curso= cursoRepository.findById(idCurso).orElseThrow(()->new RuntimeException("El id  del curso es invalido") );

        if(estudiante.esMayorEdad()){
        Inscripcion inscripcion=new Inscripcion(null,null,null,estudiante,curso);
            inscripcionRepository.save(inscripcion);
        }
        
    }
}
