package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDto;
import com.example.demo.service.InscripcionService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/inscripcion")


public class ControllerInscripcion {
    
    @Autowired
    InscripcionService inscripcionService;
    
    @PostMapping
    public InscripcionDto persistirInscripcion(@RequestBody InscripcionDto inscripcionDto){
        
        return inscripcionService.persistirInscripcion(inscripcionDto);
    }
    //ultimo punto del modulo 3
    @PostMapping("/inscripcionConIds/{idCurso}-{idEstudiante}")
    public void persistirInscripcionIds(@PathVariable Long idCurso, @PathVariable Long  idEstudiante){
        
         inscripcionService.registrarInscripcionModuloTres(idCurso,idEstudiante);
    }
    @GetMapping
    public List<InscripcionDto> getInscripciones(){
        
        return inscripcionService.getInscripciones();
    }
}
