package com.example.demo.repository;

import java.util.List;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;

public interface  InscripcionRepositoryCustom {
    public List<Inscripcion> findAllInscripcionNativa(Estado estado);
}
