package com.example.demo.domain;


import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.GregorianCalendar;

import jakarta.annotation.Generated;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import jakarta.persistence.JoinColumn;
@Entity
@Table(name= "estudiante")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String apellido;
    private String email;
    private int dni;
    private Date fechaNacimiento;

    
    private int getEdad(){

        Calendar today = Calendar.getInstance();
        Calendar fechaNac = new GregorianCalendar();
        fechaNac.setTime(this.fechaNacimiento);
        int diffYear = today.get(Calendar.YEAR) - fechaNac.get(Calendar.YEAR);
        int diffMonth = today.get(Calendar.MONTH) - fechaNac.get(Calendar.MONTH);
        int diffDay = today.get(Calendar.DAY_OF_MONTH) - fechaNac.get(Calendar.DAY_OF_MONTH);
        // Si está en ese año pero todavía no los ha cumplido
        if (diffMonth < 0 || (diffMonth == 0 && diffDay < 0)) {
            diffYear = diffYear - 1;
        }
        return diffYear;
    }
    public boolean esMayorEdad(){
        
        if(this.getEdad()>=18){
            return true;
        }

        return false;
    }
}
