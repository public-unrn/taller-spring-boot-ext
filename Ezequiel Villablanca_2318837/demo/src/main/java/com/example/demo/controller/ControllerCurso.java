package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.CursoDto;
import com.example.demo.service.CursoService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/curso")
public class ControllerCurso {
    
@Autowired
private CursoService cursoService;

@PostMapping
public CursoDto persistirCurso(@RequestBody CursoDto cursoDto){
    
    return cursoService.persistirCurso(cursoDto);
}
}
