package com.example.demo.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.domain.Curso;

public interface CursoRepository extends JpaRepository<Curso,Long>{
    
    List<Curso> findAllByOrderByIdAsc();

    
     //punto f listar cursos despues de “01/02/2020”
    @Query(value = "SELECT c FROM Curso c WHERE c.fechaInicio IS NOT NULL AND c.fechaInicio >= :fecha  ")
    List<Curso> findAllCursosMayorAFecha(@Param("fecha") LocalDate fecha);
    //lista cursos despeus de una fecha
    List<Curso>  findAllByFechaInicioIsGreaterThan(LocalDate fecha);
    
}
