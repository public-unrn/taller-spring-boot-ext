package com.example.demo;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;

import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;


import com.example.demo.repository.InscripcionRepository;




@SpringBootApplication
public class DemoApplication {

	/*@Autowired
	IPeliculaRepository peliculaRepository;
	@Autowired
	ActorRepository actorRepository;*/
	@Autowired
	private InscripcionRepository inscripcionRepository;
    @Autowired
	private CursoRepository cursoRepository;
    @Autowired
	private EstudianteRepository estudianteRepository;
	public static void main(String[] args) {
		System.out.println("hoaaa");
		SpringApplication.run(DemoApplication.class, args);
	}
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {


			//estudiantes paginados
			Pageable sortedByDniAsc = PageRequest.of(1, 5, Sort.by(Sort.Direction.ASC,"dni"));
			Page<Estudiante> estudiantes2= estudianteRepository.findAll(sortedByDniAsc);
			Pageable sortedByDniAsc2 = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC,"dni"));
			Page<Estudiante> estudiantes3= estudianteRepository.findAll(sortedByDniAsc2);
			//System.out.println(estudiantes);
			/*Estudiante estudiante =new Estudiante(null,"keko","romero","vsadkoasdko@hotmail.com",41620505,new Date());
			Estudiante estudiante2 =new Estudiante(null,"keko2","romero2","vsadkoasdko@hotmai213l.com",41620505,new Date());
			Curso curso=new Curso(null, "keko", "asdsadsa", java.sql.Date.valueOf(LocalDate.now()),java.sql.Date.valueOf(LocalDate.now()));
			this.estudianteRepository.save(estudiante);
			this.estudianteRepository.save(estudiante2);
			
			
			this.cursoRepository.save(curso);
			inscripcionRepository.save(new Inscripcion(null,new Date(),Estado.RECHAZADO,estudiante,curso));
			
			/*List <Actor> actores=new ArrayList<Actor>();
			Pelicula pelicula= new Pelicula(null,"titulo",1,"descripcion",1,actores);
			Pelicula peliculabd=peliculaRepository.save(pelicula);
			Actor actor = actorRepository.save(new Actor(null,"hola",peliculabd));
			Actor actor2 = actorRepository.save(new Actor(null,"hola2",peliculabd));
			
			Optional<Pelicula> pelicula2=peliculaRepository.findById(peliculabd.getId());
			System.out.println(pelicula2.toString());
			/*peliculaRepository.save(new Pelicula(null,"titulo",1,"sdasdopsak",10));
			
			*/
		};

	}
}
