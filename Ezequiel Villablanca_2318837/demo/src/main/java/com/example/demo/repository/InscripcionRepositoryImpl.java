package com.example.demo.repository;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.TypedQuery;
public class InscripcionRepositoryImpl implements InscripcionRepositoryCustom{
   

    @PersistenceContext
    EntityManager entityManager;
    public List<Inscripcion> findAllInscripcionNativa(Estado estado){
    try {
       
        String query = "select i FROM Inscripcion i";
        List<Inscripcion> inscripcion = entityManager.createQuery(query).getResultList();
        return inscripcion;
        
    } catch (Exception e) {
        throw new RuntimeException(e);
    }

    }
}