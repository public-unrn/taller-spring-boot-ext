package com.example.demo.controller;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;

import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;









@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class ControllerApi {

 
	
    @Autowired
	private InscripcionRepository inscripcionRepository;
    @Autowired
	private CursoRepository cursoRepository;
    @Autowired
	private EstudianteRepository estudianteRepository;
   /* @RequestMapping("/pelicula")
    public Pelicula  getPelicula(){
        
        return  pelicula;
    }
*/
/*@GetMapping("/cursos")
	public List<Curso> listarCursos(){
		return cursoRepository.findAllByOrderByIdAsc();
	}*/
    @GetMapping("/cursos")
	public List<Curso> listarCursosDespuesDeTalFecha(){
		return cursoRepository.findAllCursosMayorAFecha(LocalDate.of(2020, 1, 2));
	}
    @GetMapping("/cursos2")
	public List<Curso> listarCursosDespuesDeTalFecha2(){
       //Date fechaParametrizada= java.sql.Date.valueOf(LocalDate.of(2020, 1, 2));
		return cursoRepository.findAllByFechaInicioIsGreaterThan(LocalDate.of(2020, 1, 2));
	}
@GetMapping("/inscripciones")
public List<Inscripcion> listarInscripciones(){
    System.out.println(inscripcionRepository.findAll());
    
    List<Inscripcion> inscripcion=inscripcionRepository.findAllByEstadoLike(Estado.RECHAZADO);
    
    return  inscripcion;
}
@GetMapping("/inscripcionesnativa")
public List<Inscripcion> listarInscripcionesNativa(){
    System.out.println(inscripcionRepository.findAll());
    
    List<Inscripcion> inscripcion=inscripcionRepository.findAllInscripcionNativa(Estado.RECHAZADO);
    
    return  inscripcion;
}
@GetMapping("/estudiantes")
public List<Estudiante> listarEstudiantes(){
    return estudianteRepository.findAll();
}

@GetMapping("/estudiantemayor")
public List<Estudiante> listarEstudiantesMayor(){
    
    return estudianteRepository.findAllEstudianteMayorDni();
   
}
    @GetMapping("/curso/{id}")
    public ResponseEntity<Curso> findCurso(@PathVariable Long id){

        /*Estudiante estudiante= this.estudianteRepository.save(new Estudiante());
        Curso curso=this.cursoRepository.save(new Curso());
        */
        
        Curso curso = cursoRepository.findById(id)
				.orElseThrow(() -> new RuntimeException("Instructor con el ID : " + id + " no encontrado"));
		return ResponseEntity.ok().body(curso);
       
        
        
    }


   /*@PostMapping("/curso")
    public Curso guardarCurso(@Valid @RequestBody Curso curso){
        

      return this.cursoRepository.save(curso);
    } */ 
}
