package com.example.demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDto;
import com.example.demo.repository.EstudianteRepository;

@Service
public class EstudianteService {
    @Autowired
    EstudianteRepository estudianteRepository;

    public EstudianteDto persistirEstudiante(EstudianteDto estudianteDto){
        Estudiante estudiante= new Estudiante(null, estudianteDto.getNombre(),estudianteDto.getApellido(), estudianteDto.getEmail(),estudianteDto.getDni(), estudianteDto.getFechaNacimiento());
        estudianteRepository.save(estudiante);


        return estudianteDto;
    }

    public List<EstudianteDto> obtenerEstudiantes(){

        List<Estudiante> estudiantes=estudianteRepository.findAll();

       List<EstudianteDto> estudianteDto= estudiantes.stream().map(e-> new EstudianteDto(e.getId(), e.getNombre(), e.getApellido(), e.getEmail(), e.getDni(), e.getFechaNacimiento())).collect(Collectors.toList());
        return estudianteDto;
    }


    public List<EstudianteDto> getEstudiantesPaginadosAsc(){
        Pageable sortedByDniAsc = PageRequest.of(0, 2, Sort.by(Sort.Direction.ASC,"dni"));
        Page<Estudiante> estudiantes= estudianteRepository.findAll(sortedByDniAsc);

       List<EstudianteDto> estudianteDto= estudiantes.stream().map(e-> new EstudianteDto(e.getId(), e.getNombre(), e.getApellido(), e.getEmail(), e.getDni(), e.getFechaNacimiento())).collect(Collectors.toList());
        return estudianteDto;
        
    }
    public List<EstudianteDto> registrarEstudianteMayorEdad(){

        List<Estudiante> estudiantes=estudianteRepository.findAll();

       List<EstudianteDto> estudianteDto= estudiantes.stream().map(e-> new EstudianteDto(e.getId(), e.getNombre(), e.getApellido(), e.getEmail(), e.getDni(), e.getFechaNacimiento())).collect(Collectors.toList());
        return estudianteDto;
    }
    public EstudianteDto obtenerEstudiante(Long id){
        Estudiante estudiante=estudianteRepository.findById(id).orElse(null);
        EstudianteDto estudianteDto=new EstudianteDto(id,estudiante.getNombre(), estudiante.getApellido(),estudiante.getEmail(),estudiante.getDni(), estudiante.getFechaNacimiento());
        return estudianteDto;
        
    }
    public void eliminarEstudiante(Long id){
        estudianteRepository.deleteById(id);
    }
    public EstudianteDto editarEstudiante(Long id, EstudianteDto estudianteDto){
       Estudiante estudiante= new Estudiante(id,estudianteDto.getNombre(), estudianteDto.getApellido(), estudianteDto.getEmail(), estudianteDto.getDni(), estudianteDto.getFechaNacimiento());
        estudianteRepository.save(estudiante);
        return estudianteDto;
    }
    //public EstudianteDto editarEstudiante()
}
