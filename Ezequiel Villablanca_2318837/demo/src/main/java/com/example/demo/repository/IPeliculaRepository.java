package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.domain.Inscripcion;
import com.example.demo.domain.Pelicula;

public interface IPeliculaRepository extends JpaRepository<Pelicula,Long>{
    
    
}