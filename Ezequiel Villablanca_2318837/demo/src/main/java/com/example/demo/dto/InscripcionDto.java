package com.example.demo.dto;

import java.util.Date;

import com.example.demo.domain.Estado;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class InscripcionDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Date fechaInscripcion;
    @Enumerated(EnumType.STRING)
    private Estado estado;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name="estudiante_id")
    private EstudianteDto estudiante;
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name="curso_id")
    private CursoDto curso;

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Date getFechaInscripcion() {
        return fechaInscripcion;
    }
    public void setFechaInscripcion(Date fechaInscripcion) {
        this.fechaInscripcion = fechaInscripcion;
    }
    public Estado getEstado() {
        return estado;
    }
    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    public EstudianteDto getEstudiante() {
        return estudiante;
    }
    public void setEstudiante(EstudianteDto estudiante) {
        this.estudiante = estudiante;
    }
    public CursoDto getCurso() {
        return curso;
    }
    public void setCurso(CursoDto curso) {
        this.curso = curso;
    }
}
