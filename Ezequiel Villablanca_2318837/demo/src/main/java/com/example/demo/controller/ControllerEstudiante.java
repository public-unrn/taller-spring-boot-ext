package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.EstudianteDto;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.service.EstudianteService;
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/estudiante")
public class ControllerEstudiante {
@Autowired
private EstudianteService estudianteService;

    @PostMapping
public EstudianteDto persistirEstudiante(@RequestBody EstudianteDto estudianteDto){
    
    return estudianteService.persistirEstudiante(estudianteDto);
}
@GetMapping
public List<EstudianteDto> obtenerEstudiantes(){
    
    return estudianteService.obtenerEstudiantes();
}

@GetMapping("/estudiantespaginados")
public List<EstudianteDto> getEstudiantesPaginados(){
    
    return estudianteService.getEstudiantesPaginadosAsc();
}
@GetMapping("/{id}")
public EstudianteDto obtenerEstudiante(@PathVariable Long id){
    
    return estudianteService.obtenerEstudiante(id);
}
@DeleteMapping("/{id}")
public void eliminarEstudiante(@PathVariable Long id){
    
    estudianteService.eliminarEstudiante(id);
}
@PutMapping("/{id}")
public EstudianteDto editarEstudiante(@PathVariable Long id,@RequestBody EstudianteDto estudianteDto ){

    return estudianteService.editarEstudiante(id, estudianteDto);
}
}
