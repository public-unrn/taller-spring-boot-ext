package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.domain.Curso;
import com.example.demo.dto.CursoDto;
import com.example.demo.repository.CursoRepository;

@Service
public class CursoService {
    
    @Autowired
    private CursoRepository cursoRepository;

    public CursoDto persistirCurso(CursoDto cursoDto){
        Curso curso=new Curso(null,
        cursoDto.getNombre(),
        cursoDto.getDescripcion(),
        cursoDto.getFechaInicio(),
        cursoDto.getFechaFin());


        cursoRepository.save(curso);
        return cursoDto;
    }

}
