package com.example.demo.dto;



import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
@Data
@AllArgsConstructor
public class CursoDto {

    private Long id;
private String nombre;
private String descripcion;
private Date fechaInicio;
private Date fechaFin;
    
}
