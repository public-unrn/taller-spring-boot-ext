package com.example.demo.controller;

import com.example.demo.dto.InscripcionDTO;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/incripciones")
public class InscripcionController {

    @Autowired
    private InscripcionService inscripcionService;

    @PostMapping
    public void save(@RequestBody InscripcionDTO inscripcionDTO){
        inscripcionService.createInscripcion(inscripcionDTO.getEstudianteId(), inscripcionDTO.getCursoId());
    }

    @GetMapping("/{status1}/{status2}")
    public List<InscripcionDTO> findByEstados(@PathVariable String status1, @PathVariable String status2){
        return inscripcionService.findByDosEstados(status1, status2);
    }

    @GetMapping("/{status}")
    public List<InscripcionDTO> findByEstado(@PathVariable String status){
        return inscripcionService.findByEstado(status);
    }

    @GetMapping("/searchNativo/{estado}")
    public List<InscripcionDTO> findByEstadoSearchNativo(@RequestParam String estado){
        return inscripcionService.findByEstadoSearchNativo(estado);
    }
}
