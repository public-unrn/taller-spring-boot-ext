package com.example.demo.controller;

import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO save(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.saveEstuidante(estudianteDTO);
    }

    @GetMapping
    public List<EstudianteDTO> allStudients(){
        return estudianteService.findAll();
    }

    @GetMapping("/{id}")
    public EstudianteDTO find(@PathVariable Long id){
        return estudianteService.find(id);
    }

    @GetMapping("/{id}/{apellido}")
    public List<EstudianteDTO> findByDniAndApellido(@PathVariable Long dni, @PathVariable String apellido){
        return estudianteService.findDniAndApellido(dni, apellido);
    }

    @PutMapping("/{id}")
    public EstudianteDTO update(@PathVariable Long id, @RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.update(id, estudianteDTO);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
        estudianteService.delete(id);
    }
}
