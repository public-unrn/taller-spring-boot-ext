package com.example.demo.repository;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EstudianteRespository extends JpaRepository<Estudiante, Long> {


    @Query("SELECT e FROM Estudiante e")
    List<Curso> findAllCursos();

    @Query("SELECT e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> findByDniYApellido();

    //List<Curso> findAllCurso();

    @Query("SELECT e FROM Estudiante e WHERE e.dni > :DNI AND e.apellido = :APELLIDO")
    List<Estudiante> findByDniAndApellido(@Param("DNI") Long dni, @Param("APELLIDO") String apellido);

}
