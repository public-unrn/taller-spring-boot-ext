package com.example.demo.repository;

import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {


    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :status1 OR i.estado = :status2")
    List<Inscripcion> findByDosEstados(@Param("status1") EstadoInscripcion status1, @Param("status2") EstadoInscripcion status2);


    @Query("SELECT i FROM Inscripcion i WHERE i.estado = :status")
    List<Inscripcion> findByEstado(@Param("status")EstadoInscripcion status);


    @Query(value = "SELECT * FROM Inscripciones i WHERE i.estado :status", nativeQuery = true)
    List<Inscripcion> findByEstadoNativo(@Param("status")EstadoInscripcion status);

}
