package com.example.demo.domain;

public enum EstadoInscripcion {

    ACEPTADO, RECHAZADO, PENDIENTE
}
