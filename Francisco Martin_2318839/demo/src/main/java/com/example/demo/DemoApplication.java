package com.example.demo;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRespository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.EstudianteService;
import com.example.demo.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursosRepo;
	@Autowired
	EstudianteRespository estudiantesRepo;
	@Autowired
	InscripcionRepository inscripcionesRepo;

	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx){
		return args -> {

			this.generarInscripciones(this.crearYCargarEstudiantes(),this.crearYCargarCursos());

		};

	}

	private List<Estudiante> crearYCargarEstudiantes(){

		List<Estudiante> estudiantes = new ArrayList<Estudiante>();

		Estudiante juan = new Estudiante(null,10293847L,"juan@abc.com",LocalDate.of(1990,3,15),"Romero",33,"juan");
		Estudiante pedro = new Estudiante(null,19287364L, "pedro@abc.com", LocalDate.of(1985,7,22), "Diaz",38, "pedro");
		Estudiante diego = new Estudiante(null,28192745L,"diego@abc.com", LocalDate.of(1982,1,30), "Gutierrez",41,"diego");
		Estudiante carlos = new Estudiante(null, 41735987L,"carlos@abc", LocalDate.of(1993,6,12),"Romero", 30,"carlos");

		estudiantes.add(juan = estudiantesRepo.saveAndFlush(juan));
		estudiantes.add(pedro = estudiantesRepo.saveAndFlush(pedro));
		estudiantes.add(diego = estudiantesRepo.saveAndFlush(diego));
		estudiantes.add(carlos = estudiantesRepo.saveAndFlush(carlos));

		return estudiantes;
	}

	private List<Curso> crearYCargarCursos(){

		List<Curso> cursos = new ArrayList<Curso>();

		Curso spring = new Curso(null, "Este es un curso de Spring", LocalDate.now(), LocalDate.of(2023,10,11),"spring-boot");
		Curso concurrente = new Curso(null, "curso de programacion concurrente", LocalDate.of(2019,3,12),LocalDate.of(2019,06,27), "concurrente");
		Curso baseDatos	= new Curso(null, "curso sobre base de datos relacioal", LocalDate.of(2018,8,07), LocalDate.of(2018,11,24),"base-datos");

		cursos.add(spring = cursosRepo.saveAndFlush(spring));
		cursos.add(concurrente = cursosRepo.saveAndFlush(concurrente));
		cursos.add(baseDatos = cursosRepo.saveAndFlush(baseDatos));

		return cursos;

	}

	private void generarInscripciones(List<Estudiante> estudiantes, List<Curso> cursos){

		for (Estudiante e: estudiantes) {
			for (Curso c: cursos) {
				Inscripcion nuevaInscripcion = new Inscripcion(null, obtenerEstadoAlAzar(), c, e, LocalDate.now());
				inscripcionesRepo.save(nuevaInscripcion);
			}
		}

	}

	private static EstadoInscripcion obtenerEstadoAlAzar() {

		Random random = new Random();
		int indiceAleatorio = random.nextInt(EstadoInscripcion.values().length);

		return EstadoInscripcion.values()[indiceAleatorio];
	}
}
