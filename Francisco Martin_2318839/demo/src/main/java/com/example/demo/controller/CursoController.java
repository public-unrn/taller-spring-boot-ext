package com.example.demo.controller;

import com.example.demo.dto.CursoDTO;
import com.example.demo.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cursos")
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @PostMapping
    public CursoDTO save(@RequestBody CursoDTO cursoDto){
        return cursoService.saveCurso(cursoDto);
    }

    @GetMapping
    public List<CursoDTO> allCursos(){
        return cursoService.findAll();
    }

    @GetMapping("/{fecha}")
    public List<CursoDTO> findByFechaInicio(@PathVariable String fecha){
        return cursoService.findAfterFechaInit(fecha);
    }
}
