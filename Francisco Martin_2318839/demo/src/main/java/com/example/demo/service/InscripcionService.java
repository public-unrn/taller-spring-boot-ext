package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRespository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import java.util.stream.Collectors;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Validated
public class InscripcionService {

    @Autowired
    private InscripcionRepository inscripcionRepository;
    @Autowired
    private EstudianteRespository estudianteRespository;
    @Autowired
    private CursoRepository cursoRepository;

@Transactional
    public void createInscripcion(@Positive(message = "El 'estudianteId' no puede ser negativo.") Long estudianteId,
                          @Positive(message = "El 'cursoId' no puede ser negativo.") Long cursoId){

        Objects.requireNonNull(estudianteId, "Proporcione un id no nulo para estudianteId");
        Objects.requireNonNull(cursoId, "Proporcione un id no nulo para cursoId");

        Estudiante estudiante = estudianteRespository.findById(estudianteId)
                .orElseThrow(()-> new RuntimeException("El id del estudiante no existe"));


        if(estudiante.getEdad() < 18){
            throw new RuntimeException("El estudiante es menor de edad");
        }

        Curso curso = cursoRepository.findById(cursoId)
                .orElseThrow(()-> new RuntimeException("El id del curso no existe"));

        inscripcionRepository.save(new Inscripcion(null, obtenerEstadoAlAzar(), curso, estudiante, LocalDate.now()));

    }

    public List<InscripcionDTO> findByEstado(String status){
        return this.mapInscripcionToDTOs(inscripcionRepository.findByEstado(EstadoInscripcion.valueOf(status)));
    }

    public List<InscripcionDTO> findByEstadoSearchNativo(String estado){
        return this.mapInscripcionToDTOs(inscripcionRepository.findByEstadoNativo(EstadoInscripcion.valueOf(estado)));
    }

    public List<InscripcionDTO> findByDosEstados(String status1, String status2){

        return this.mapInscripcionToDTOs(inscripcionRepository.findByDosEstados(EstadoInscripcion.valueOf(status1),
                EstadoInscripcion.valueOf(status2)));
    }

    private List<InscripcionDTO> mapInscripcionToDTOs(List<Inscripcion> inscripciones) {
        return inscripciones
                .stream()
                .map(inscripcion -> new InscripcionDTO(inscripcion.getEstado().toString(),
                        inscripcion.getCurso().getId(),
                        inscripcion.getEstudiante().getId(),
                        inscripcion.getFechaInscripcion()))
                .collect(Collectors.toList());
    }

    private static EstadoInscripcion obtenerEstadoAlAzar() {

        Random random = new Random();
        int indiceAleatorio = random.nextInt(EstadoInscripcion.values().length);

        return EstadoInscripcion.values()[indiceAleatorio];
    }
}
