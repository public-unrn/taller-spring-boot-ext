package com.example.demo.service;

import com.example.demo.domain.Curso;
import com.example.demo.domain.Estudiante;
import com.example.demo.dto.CursoDTO;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CursoService {

    @Autowired
    private CursoRepository cursoRepository;

    public CursoDTO saveCurso(CursoDTO cursoDto){
        Curso newCurso = new Curso(null,
                cursoDto.getDescripcion(),
                cursoDto.getFechaInicio(),
                cursoDto.getFechaFin(),
                cursoDto.getNombre());

        cursoRepository.save(newCurso);

        return cursoDto;
    }

    public List<CursoDTO> findAll(){

        return this.mapCursoToDTOs(cursoRepository.findAllCursos());
    }

    public List<CursoDTO> findAfterFechaInit(String fechaInicio){

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate fecha = LocalDate.parse(fechaInicio, formatter);

        return this.mapCursoToDTOs(cursoRepository.findByFechaInicioAfter(fecha));
    }


    private List<CursoDTO> mapCursoToDTOs(List<Curso> cursos) {
        return cursos
                .stream()
                .map(curso -> new CursoDTO(
                        curso.getId(),
                        curso.getDescripcion(),
                        curso.getFechaInicio(),
                        curso.getFechaFin(),
                        curso.getNombre()))
                .collect(Collectors.toList());
    }
}
