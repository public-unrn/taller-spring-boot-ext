package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRespository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRespository estudianteRespository;

    public EstudianteDTO saveEstuidante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(null,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEdad(),
                estudianteDTO.getNombre());

        estudianteRespository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){

        return this.mapEstudiantesToDTOs(estudianteRespository.findAll());
    }

    public EstudianteDTO find(Long estudianteId){
        estudianteRespository.findById(estudianteId);

        Objects.requireNonNull(estudianteId, "Proporcione un id no nulo para estudianteId");

        Estudiante estudiante = estudianteRespository.findById(estudianteId)
                .orElseThrow(()-> new RuntimeException("El id del estudiante no existe"));

        return new EstudianteDTO(estudiante.getDni(),
                estudiante.getEmail(),
                estudiante.getFechaDeNacimiento(),
                estudiante.getApellido(),
                estudiante.getEdad(),
                estudiante.getNombre());
    }

    public List<EstudianteDTO> findDniAndApellido(long Dni, String apellido){

        return this.mapEstudiantesToDTOs(estudianteRespository.findByDniAndApellido(Dni, apellido));
    }

    public EstudianteDTO update(Long id, EstudianteDTO estudianteDTO){

        Estudiante estudiante = new Estudiante(id,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEdad(),
                estudianteDTO.getNombre());

        estudianteRespository.save(estudiante);

        return estudianteDTO;
    }

    public void delete(Long id){
        estudianteRespository.deleteById(id);
    }


    public Page<EstudianteDTO> findAllByPaginacion(Pageable pageable){
        return new PageImpl<>(this.mapEstudiantesToDTOs(estudianteRespository.findAll(pageable).getContent()));
    }

    private List<EstudianteDTO> mapEstudiantesToDTOs(List<Estudiante> estudiantes) {
        return estudiantes
                .stream()
                .map(estudiante -> new EstudianteDTO(
                        estudiante.getDni(),
                        estudiante.getEmail(),
                        estudiante.getFechaDeNacimiento(),
                        estudiante.getApellido(),
                        estudiante.getEdad(),
                        estudiante.getNombre()))
                .collect(Collectors.toList());
    }
}
