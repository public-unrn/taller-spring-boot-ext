package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Entity
@Table(name = "Curso")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Curso_id")
    private Long id;

    @Column(name = "Descripcion")
    private String descripcion;

    @Column(name = "Fecha Comienzo")
    private LocalDate fechaInicio;

    @Column(name = "Fecha Fin")
    private LocalDate fechaFin;

    @Column(name = "Nombre")
    private String nombre;
}
