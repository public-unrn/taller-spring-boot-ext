package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "Estudiante")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Estudiante_id")
    private Long id;

    @Column(name = "DNI")
    private Long dni;

    @Column(name = "Email")
    private String email;

    @Column(name = "Fecha De Nacimiento")
    private LocalDate fechaDeNacimiento;

    @Column(name = "Apellido")
    private String apellido;

    @Column(name = "Edad")
    private Integer edad;

    @Column(name = "Nombre")
    private String nombre;
}
