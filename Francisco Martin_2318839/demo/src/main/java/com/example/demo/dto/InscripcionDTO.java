package com.example.demo.dto;

import com.example.demo.domain.Curso;
import com.example.demo.domain.EstadoInscripcion;
import com.example.demo.domain.Estudiante;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
@Data
@AllArgsConstructor
public class InscripcionDTO {

    private String estado;
    private Long cursoId;
    private Long estudianteId;
    private LocalDate fechaInscripcion;
}
