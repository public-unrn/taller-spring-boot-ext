package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "Inscripciones")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "Estado")
    private EstadoInscripcion estado;


    @ManyToOne
    @JoinColumn(name = "Curso_id")
    private Curso curso;


    @ManyToOne
    @JoinColumn(name = "Estudiante_id")
    private Estudiante estudiante;

    @Column(name = "Fecha Inscripcion")
    private LocalDate fechaInscripcion;
}
