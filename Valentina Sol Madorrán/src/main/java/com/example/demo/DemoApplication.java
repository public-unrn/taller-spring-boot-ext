package com.example.demo;

import com.example.demo.controller.StudentController;
import com.example.demo.domain.Curse;
import com.example.demo.domain.Student;
import com.example.demo.dto.StudentDTO;
import com.example.demo.repository.CurseRepository;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CurseRepository curseRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	InscriptionRepository inscriptionRepository;

	@Autowired
	StudentController studentController;

	@Autowired
	InscriptionService inscriptionService;

   @Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			this.curseRepository.save(new Curse(null, "BD1", "base de datos 1", LocalDate.of(2023,03, 5), LocalDate.of(2023, 06, 23)));
			this.curseRepository.save(new Curse(null, "BD2", "base de datos 2", LocalDate.of(2023,8, 5), LocalDate.of(2023, 11, 23)));
			this.curseRepository.findAll();

			this.studentController.saveStudent(new StudentDTO("Juan", "Perez", "jperez@gmail", "12331882", LocalDate.of(1990, 03, 23)));
			this.studentController.saveStudent(new StudentDTO("Juan", "Romero", "juanr@gmail", "21000000", LocalDate.of(1990, 03, 23)));


			 this.studentRepository.findAll();
			 this.studentRepository.findAllByDniGreaterThanEqualAndLastnameEquals("21000000", "Romero");

			 this.curseRepository.findCursesByStartDateOlderThan(LocalDate.of(2020, 02, 01));

			this.studentRepository.findPageableStudentByDniAndLastname(PageRequest.of(1,5, Sort.by(Sort.Direction.ASC, "dni")));
			this.studentRepository.findPageableStudentByDniAndLastname(PageRequest.of(0,2, Sort.by(Sort.Direction.ASC, "dni")));
			Student student = new Student(1L, "Juan", "Perez", "jperez@gmail", "12331882", LocalDate.of(1990, 03, 23));
			System.out.println(student.isLegalAge());

			this.inscriptionService.registerInscription(1L,1L);
		};
	}

}
