package com.example.demo.service;

import com.example.demo.domain.Student;
import com.example.demo.dto.StudentDTO;
import com.example.demo.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public StudentDTO saveStudent(StudentDTO studentDTO) {

        Student student = new Student(
                null,
                studentDTO.getName(),
                studentDTO.getLastname(),
                studentDTO.getEmail(),
                studentDTO.getDni(),
                studentDTO.getBirthDate()
        );

        studentRepository.save(student);
        return studentDTO;
    }

    public StudentDTO updateStudent(StudentDTO studentDTO, Long id) {

        Student student = new Student(
                id,
                studentDTO.getName(),
                studentDTO.getLastname(),
                studentDTO.getEmail(),
                studentDTO.getDni(),
                studentDTO.getBirthDate()
        );

        studentRepository.save(student);
        return studentDTO;
    }

    public List<StudentDTO> findAll() {
        return studentRepository.findAllStudents().stream().map(
                student -> new StudentDTO(
                        student.getName(),
                        student.getLastname(),
                        student.getEmail(),
                        student.getDni(),
                        student.getBirthDate()
                )
        ).toList();
    }

    public StudentDTO findById(Long id) {
        Optional<Student> studentOptional = studentRepository.findById(id);

        if (studentOptional.isEmpty()) {
            throw new RuntimeException("Estudiante no encontrado");
        }

        Student student = studentOptional.get();
        return new StudentDTO(
                student.getName(),
                student.getLastname(),
                student.getEmail(),
                student.getDni(),
                student.getBirthDate()
        );
    }

    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }
}
