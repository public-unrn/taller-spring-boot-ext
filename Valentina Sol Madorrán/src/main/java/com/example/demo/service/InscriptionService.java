package com.example.demo.service;

import com.example.demo.domain.Curse;
import com.example.demo.domain.Inscription;
import com.example.demo.domain.Student;
import com.example.demo.dto.CurseDTO;
import com.example.demo.dto.InscriptionDTO;
import com.example.demo.repository.CurseRepository;
import com.example.demo.repository.InscriptionRepository;
import com.example.demo.repository.StudentRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Optional;

@Service
@Validated
public class InscriptionService {

    @Autowired
    private InscriptionRepository inscriptionRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CurseRepository curseRepository;

    public InscriptionDTO saveInscription(InscriptionDTO inscriptionDTO) {

        Student student = studentRepository.findById(inscriptionDTO.getStudent()).get();
        Curse curse = curseRepository.findById(inscriptionDTO.getCurse()).get();

        Inscription inscription = new Inscription(
                null,
                curse,
                student,
                inscriptionDTO.getInscriptionDate(),
                inscriptionDTO.getState()
        );
        inscriptionRepository.save(inscription);
        return inscriptionDTO;

    }

    @Transactional
    public void registerInscription(@NotNull @Positive(message = "El id no puede ser negativo") Long curseId, @NotNull @Positive(message = "El id no puede ser negativo")  Long studentId) {

        Curse curse = curseRepository.findById(curseId).orElseThrow(() -> new RuntimeException("El curso no existe"));
        Student student = studentRepository.findById(studentId).orElseThrow(() -> new RuntimeException("El estudiante no existe"));


        if (!student.isLegalAge()) {
            throw new RuntimeException("El estudiante no tiene la edad suficiente para inscribirse en el curso");
        }
        Inscription inscription = new Inscription(null, curse, student, LocalDate.now(), "REGISTERED");
        inscriptionRepository.save(inscription);

      //  throw new RuntimeException("El estudiante no se pudo inscribir en el curso");

    }
}
