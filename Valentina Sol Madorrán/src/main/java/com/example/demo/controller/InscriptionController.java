package com.example.demo.controller;

import com.example.demo.dto.InscriptionDTO;
import com.example.demo.service.InscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class InscriptionController {

    @Autowired
    InscriptionService inscriptionService;

    @PostMapping("/inscription")
    public InscriptionDTO saveInscription(@RequestBody InscriptionDTO inscriptionDTO) {
        return inscriptionService.saveInscription(inscriptionDTO);
    }
}
