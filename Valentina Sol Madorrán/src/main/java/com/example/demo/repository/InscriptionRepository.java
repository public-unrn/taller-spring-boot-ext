package com.example.demo.repository;

import com.example.demo.domain.Inscription;
import com.example.demo.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscriptionRepository extends JpaRepository<Inscription, Long> {

    @Query("SELECT i FROM Inscription i WHERE i.state = 'PENDING' OR i.state = 'REJECTED'")
    List<Inscription> findInscriptionPendingOrRejected();

    List<Inscription> findByState(String state);

    @Query(value = "SELECT * FROM inscription i WHERE i.state = :state", nativeQuery = true)
    List<Inscription> findInscriptionByStateNative(@Param("state") String state);
}
