package com.example.demo.domain;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Inscription")
public class Inscription {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "curse_id")
    private Curse curse;

    @ManyToOne
    @JoinColumn(name = "studente_id")
    private Student student;
    
    @Column(name = "inscription_date")
    private LocalDate inscriptionDate;
    
    @Column(name = "state")
    private String state;

}

    


