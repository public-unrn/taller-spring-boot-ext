package com.example.demo.service;

import com.example.demo.domain.Curse;
import com.example.demo.domain.Student;
import com.example.demo.dto.CurseDTO;
import com.example.demo.dto.StudentDTO;
import com.example.demo.repository.CurseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurseService {

    @Autowired
    private CurseRepository curseRepository;

    public CurseDTO saveCurse(CurseDTO curseDTO) {

        Curse curse = new Curse(
                null,
                curseDTO.getName(),
                curseDTO.getDescription(),
                curseDTO.getStartDate(),
                curseDTO.getEndDate()
        );

        curseRepository.save(curse);
        return curseDTO;
    }
}
