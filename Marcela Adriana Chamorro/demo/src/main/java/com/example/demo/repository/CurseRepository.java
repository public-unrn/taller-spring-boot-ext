package com.example.demo.repository;

import com.example.demo.domain.Curse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.util.List;

public interface CurseRepository extends JpaRepository<Curse, Long> {

    @Query("SELECT c FROM Curse c")
    List<Curse> findAllCurses();

    @Query("SELECT c FROM Curse c WHERE c.startDate = :startDate")
    List<Curse> findCursesByStartDateOlderThan(LocalDate startDate);

    List<Curse> findAll();
}
