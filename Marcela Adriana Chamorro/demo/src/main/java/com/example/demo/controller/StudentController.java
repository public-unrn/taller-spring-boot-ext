package com.example.demo.controller;

import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.demo.dto.StudentDTO;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping("/student")
    public StudentDTO saveStudent(@RequestBody StudentDTO studentDTO) {
        return studentService.saveStudent(studentDTO);
    }

    @PutMapping("/student/{id}")
    public StudentDTO updateStudent(@RequestBody StudentDTO studentDTO, @PathVariable("id") long id) {
        return studentService.updateStudent(studentDTO, id);
    }

    @GetMapping("/students")
    public List<StudentDTO> getStudents() { return studentService.findAll(); }

    @GetMapping("/student/{id}")
    public StudentDTO getStudentById(@PathVariable("id") long id) {
        return studentService.findById(id);
    }

    @DeleteMapping("/student/{id}")
    public void deleteStudent(@PathVariable("id") long id) {
        studentService.deleteById(id);
    }
}
