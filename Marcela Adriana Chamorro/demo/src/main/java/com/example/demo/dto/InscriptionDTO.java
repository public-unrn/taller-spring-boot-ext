package com.example.demo.dto;

import com.example.demo.domain.Curse;
import com.example.demo.domain.Student;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscriptionDTO {

    private Long curse;
    private Long student;
    private LocalDate inscriptionDate;
    private String state;

}
