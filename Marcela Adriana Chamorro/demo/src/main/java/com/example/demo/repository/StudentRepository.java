package com.example.demo.repository;

import com.example.demo.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends JpaRepository<Student, Long> {

    @Query("SELECT e FROM Student e")
    List<Student> findAllStudents();

    @Query("SELECT e FROM Student e WHERE e.id = :id")
   Optional<Student> findById(@Param("id") Long id);

    @Query("SELECT e FROM Student e WHERE e.dni >= :dni AND e.lastname = :lastname")
    List<Student> findStudentByDniAndLastname(@Param("dni") String dni, @Param("lastname") String lastname);

    @Query("SELECT e FROM Student e")
    Page<Student> findPageableStudentByDniAndLastname(Pageable pageable);

    List<Student> findAll();

    List<Student> findAllByDniGreaterThanEqualAndLastnameEquals(String dni, String name);

}
