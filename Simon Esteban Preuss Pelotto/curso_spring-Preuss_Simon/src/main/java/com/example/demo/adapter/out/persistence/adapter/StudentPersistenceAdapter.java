package com.example.demo.adapter.out.persistence.adapter;

import com.example.demo.adapter.out.persistence.entity.StudentEntity;
import com.example.demo.adapter.out.persistence.repository.StudentRepository;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.PersistenceAdapter;
import com.example.demo.domain.Student;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@PersistenceAdapter
@RequiredArgsConstructor
public class StudentPersistenceAdapter implements StudentPort {
    private final StudentRepository studentRepository;

    @Override
    public Optional<Student> find(Long id) {
       return studentRepository.findById(id)
               .map(StudentEntity::toDomain);
    }

    @Override
    public Student save(Student student) {
        StudentEntity mapper = new StudentEntity();

        StudentEntity result = studentRepository.save(mapper.fromDomain(student));

        return mapper.toDomain(result);
    }

    @Override
    public List<Student> findAll() {
        return studentRepository.findAll().stream()
                .map(StudentEntity::toDomain)
                .toList();
    }

    @Override
    public void delete(Long id) {
        studentRepository.deleteById(id);
    }

    @Override
    public Student update(Long id, Student student) {
        StudentEntity mapper = new StudentEntity();

        student.setId(id);
        StudentEntity result  = studentRepository.save(mapper.fromDomain(student));

        return mapper.toDomain(result);
    }
}
