package com.example.demo.application.port.out;

import com.example.demo.domain.Inscription;

public interface InscriptionPort {
    Inscription save(Inscription inscription);
}
