package com.example.demo.adapter.out.persistence.repository;

import com.example.demo.adapter.out.persistence.entity.CourseEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface CourseRepository extends JpaRepository<CourseEntity, Long> {
    List<CourseEntity> findByFechaInicioAfter(LocalDate fechaInicio);
}
