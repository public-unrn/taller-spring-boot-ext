package com.example.demo.adapter.in.api.controller;

import com.example.demo.adapter.in.api.dto.StudentDTO;
import com.example.demo.application.port.in.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Tag(name = "Estudiante")
@RequestMapping("/estudiante")
public class StudentController {

    private final AllStudentsUseCase allStudentsUseCase;
    private final DeleteStudentUseCase deleteStudentUseCase;
    private final UpdateStudentUseCase updateStudentUseCase;
    private final FindStudentUseCase findStudentUseCase;
    private final SaveStudentUseCase saveStudentUseCase;

    @PostMapping
    public ResponseEntity<StudentDTO> create(@Valid @org.springframework.web.bind.annotation.RequestBody StudentDTO studentDTO) {
        StudentDTO response = StudentDTO.fromDomain(saveStudentUseCase.save(StudentDTO.toDomain(studentDTO)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<StudentDTO>> findAll() {
        List<StudentDTO> response = allStudentsUseCase.findAll().stream()
                .map(StudentDTO::fromDomain)
                .toList();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> find(@PathVariable(name = "id") Long id) {
        StudentDTO response = StudentDTO.fromDomain(findStudentUseCase.find(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable(name = "id") Long id) {
        deleteStudentUseCase.delete(id);
        return new ResponseEntity<>("Estudiante eliminado con exito!", HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<StudentDTO> update(@PathVariable(name = "id") Long id, @Valid @org.springframework.web.bind.annotation.RequestBody StudentDTO studentDTO) {
        StudentDTO response = StudentDTO.fromDomain(updateStudentUseCase.update(id, StudentDTO.toDomain(studentDTO)));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
