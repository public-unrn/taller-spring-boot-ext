package com.example.demo.application.service.estudiante;

import com.example.demo.application.port.in.AllStudentsUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Student;
import lombok.AllArgsConstructor;

import java.util.List;

@UseCase
@AllArgsConstructor
public class AllStudentsService implements AllStudentsUseCase {

    private final StudentPort studentPort;

    @Override
    public List<Student> findAll() {
        return studentPort.findAll();
    }
}
