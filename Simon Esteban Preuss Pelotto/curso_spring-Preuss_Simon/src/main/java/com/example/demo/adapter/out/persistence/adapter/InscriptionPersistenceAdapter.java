package com.example.demo.adapter.out.persistence.adapter;

import com.example.demo.adapter.out.persistence.entity.InscriptionEntity;
import com.example.demo.adapter.out.persistence.repository.InscriptionRepository;
import com.example.demo.application.port.out.InscriptionPort;
import com.example.demo.common.PersistenceAdapter;
import com.example.demo.domain.Inscription;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class InscriptionPersistenceAdapter implements InscriptionPort {
    private final InscriptionRepository inscriptionRepository;

    @Override
    public Inscription save(Inscription inscription) {
        InscriptionEntity result = inscriptionRepository.save(InscriptionEntity.fromDomain(inscription));

        return result.toDomain(result);
    }
}
