package com.example.demo.application.port.in;

import com.example.demo.domain.Course;

public interface SaveCourseUseCase {
    Course save(Course course);
}
