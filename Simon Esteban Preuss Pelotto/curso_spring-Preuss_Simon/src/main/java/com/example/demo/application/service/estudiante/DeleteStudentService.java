package com.example.demo.application.service.estudiante;

import com.example.demo.application.port.in.DeleteStudentUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.UseCase;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class DeleteStudentService implements DeleteStudentUseCase {
    private final StudentPort studentPort;

    @Override
    public void delete(Long id) {
        studentPort.delete(id);
    }
}
