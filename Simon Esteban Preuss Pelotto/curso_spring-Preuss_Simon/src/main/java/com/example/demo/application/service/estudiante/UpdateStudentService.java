package com.example.demo.application.service.estudiante;

import com.example.demo.application.port.in.UpdateStudentUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Student;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class UpdateStudentService implements UpdateStudentUseCase {
    private final StudentPort studentPort;

    @Override
    public Student update(Long id, Student student) {
        Student result = studentPort.update(id, student);
        return result;
    }
}
