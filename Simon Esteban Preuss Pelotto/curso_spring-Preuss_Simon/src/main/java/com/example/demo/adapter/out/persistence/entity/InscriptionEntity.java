package com.example.demo.adapter.out.persistence.entity;

import com.example.demo.domain.State;
import com.example.demo.domain.Inscription;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Table(name = "inscription")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InscriptionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "fecha_inscripcion")
    private LocalDateTime fechaInscripcion;

    @Enumerated(value = EnumType.STRING)
    private State state;

    @ManyToOne
    @JoinColumn(name = "id_curso")
    private CourseEntity curso;

    @ManyToOne
    @JoinColumn(name = "id_estudiante")
    private StudentEntity estudiante;

    public static InscriptionEntity fromDomain(Inscription inscription) {
        return InscriptionEntity.builder()
                .id(inscription.getId())
                .fechaInscripcion(inscription.getFechaInscripcion())
                .state(inscription.getState())
                .curso(CourseEntity.fromDomain(inscription.getCourse()))
                .estudiante(StudentEntity.fromDomain(inscription.getStudent()))
                .build();
    }

    public static Inscription toDomain(InscriptionEntity inscripcion) {
        return Inscription.builder()
                .id(inscripcion.getId())
                .fechaInscripcion(inscripcion.getFechaInscripcion())
                .state(inscripcion.getState())
                .course(CourseEntity.toDomain(inscripcion.getCurso()))
                .student(StudentEntity.toDomain(inscripcion.getEstudiante()))
                .build();
    }
}
