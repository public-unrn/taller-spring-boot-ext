package com.example.demo.adapter.out.persistence.adapter;

import com.example.demo.adapter.out.persistence.entity.CourseEntity;
import com.example.demo.adapter.out.persistence.repository.CourseRepository;
import com.example.demo.application.port.out.CoursePort;
import com.example.demo.common.PersistenceAdapter;
import com.example.demo.domain.Course;
import lombok.RequiredArgsConstructor;

@PersistenceAdapter
@RequiredArgsConstructor
public class CoursePersistenceAdapter implements CoursePort {
    private final CourseRepository courseRepository;

    @Override
    public Course save(Course course) {
        CourseEntity mapper = new CourseEntity();

        CourseEntity result = courseRepository.save(mapper.fromDomain(course));

        return mapper.toDomain(result);
    }
}
