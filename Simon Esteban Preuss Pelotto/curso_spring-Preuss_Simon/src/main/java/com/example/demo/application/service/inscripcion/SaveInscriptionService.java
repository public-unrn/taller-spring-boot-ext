package com.example.demo.application.service.inscripcion;

import com.example.demo.application.port.in.SaveInscriptionUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.application.port.out.InscriptionPort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Student;
import com.example.demo.domain.Inscription;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class SaveInscriptionService implements SaveInscriptionUseCase {
    private final InscriptionPort inscriptionPort;
    private final StudentPort studentPort;

    @Override
    public Inscription save(Inscription inscription) {
        Optional<Student> estudianteOptional = studentPort.find(inscription.getStudent().getId());

        if(!estudianteOptional.isPresent()) {
            throw new RuntimeException("El estudiante no existe");
        }

        Student student = estudianteOptional.get();

        if(!student.esMayor()) {
            throw new RuntimeException("El estudiante debe ser mayor de edad");
        }

        Inscription result = inscriptionPort.save(inscription);

        return result;
    }
}
