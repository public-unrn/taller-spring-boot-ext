package com.example.demo.adapter.out.persistence.entity;

import com.example.demo.domain.Course;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "course")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CourseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private String descipcion;

    @Column(name = "fecha_inicio")
    private LocalDate fechaInicio;

    @Column(name = "fecha_fin")
    private LocalDate fechaFin;

    public static CourseEntity fromDomain(Course course) {
        return CourseEntity.builder()
                .id(course.getId())
                .nombre(course.getNombre())
                .descipcion(course.getDescipcion())
                .fechaInicio(course.getFechaInicio())
                .fechaFin(course.getFechaFin())
                .build();
    }

    public static Course toDomain(CourseEntity curso) {
        return Course.builder()
                .id(curso.getId())
                .nombre(curso.getNombre())
                .descipcion(curso.getDescipcion())
                .fechaInicio(curso.getFechaInicio())
                .fechaFin(curso.getFechaFin())
                .build();
    }
}
