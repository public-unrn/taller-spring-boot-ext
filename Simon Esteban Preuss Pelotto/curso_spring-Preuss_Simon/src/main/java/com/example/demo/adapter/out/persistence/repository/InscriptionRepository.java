package com.example.demo.adapter.out.persistence.repository;

import com.example.demo.adapter.out.persistence.entity.InscriptionEntity;
import com.example.demo.domain.State;
import com.example.demo.domain.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscriptionRepository extends JpaRepository<InscriptionEntity, Long> {
    List<InscriptionEntity> findByState(State state);

    @Query(value = "SELECT * FROM inscription WHERE state = :estadoParam", nativeQuery = true)
    List<InscriptionEntity> findByStateNativa(@Param("estadoParam") State state);
}
