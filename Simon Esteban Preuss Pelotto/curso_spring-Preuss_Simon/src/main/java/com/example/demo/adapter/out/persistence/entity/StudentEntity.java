package com.example.demo.adapter.out.persistence.entity;

import com.example.demo.domain.Student;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "student")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String apellido;
    private String email;
    private int edad;
    private String dni;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    public static Student toDomain(StudentEntity studentEntity) {
        return Student.builder()
                .id(studentEntity.getId())
                .nombre(studentEntity.getNombre())
                .apellido(studentEntity.getApellido())
                .email(studentEntity.getEmail())
                .dni(studentEntity.getDni())
                .edad(studentEntity.getEdad())
                .fechaNacimiento(studentEntity.getFechaNacimiento())
                .build();

    }

    public static StudentEntity fromDomain(Student student) {
        return StudentEntity.builder()
                .id(student.getId())
                .nombre(student.getNombre())
                .apellido(student.getApellido())
                .email(student.getEmail())
                .dni(student.getDni())
                .edad(student.getEdad())
                .fechaNacimiento(student.getFechaNacimiento())
                .build();
    }
}
