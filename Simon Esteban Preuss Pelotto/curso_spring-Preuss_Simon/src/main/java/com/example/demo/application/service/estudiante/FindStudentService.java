package com.example.demo.application.service.estudiante;

import com.example.demo.application.port.in.FindStudentUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Student;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@UseCase
@RequiredArgsConstructor
public class FindStudentService implements FindStudentUseCase {
    private final StudentPort studentPort;

    @Override
    public Student find(Long id) {
        Optional<Student> estudianteOptional = studentPort.find(id);

        if(!estudianteOptional.isPresent()) {
            throw new RuntimeException("El estudiante no existe");
        }

        return estudianteOptional.get();
    }
}
