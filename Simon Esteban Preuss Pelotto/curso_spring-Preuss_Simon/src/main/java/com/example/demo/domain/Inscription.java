package com.example.demo.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class Inscription {
    private Long id;
    private LocalDateTime fechaInscripcion;
    private State state;
    private Course course;
    private Student student;
}
