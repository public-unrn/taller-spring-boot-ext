package com.example.demo.application.port.in;

import com.example.demo.domain.Inscription;

public interface SaveInscriptionUseCase {
    Inscription save(Inscription inscription);
}
