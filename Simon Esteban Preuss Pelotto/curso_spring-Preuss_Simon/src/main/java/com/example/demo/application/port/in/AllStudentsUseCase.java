package com.example.demo.application.port.in;

import com.example.demo.domain.Student;

import java.util.List;

public interface AllStudentsUseCase {

    List<Student> findAll();
}
