package com.example.demo.adapter.in.api.dto;

import com.example.demo.domain.Student;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentDTO {
    private Long id;

    @NotBlank(message = "El nombre no puede estar vacío")
    private String nombre;

    @NotBlank(message = "El nombre no puede estar vacío")
    private String apellido;

    @NotBlank(message = "El nombre no puede estar vacío")
    private String email;

    @NotBlank(message = "El nombre no puede estar vacío")
    private String dni;

    @NotNull(message = "La edad es un campo requerido")
    @Min(value = 18, message = "La edad debe ser mayor o igual a 18")
    private int edad;

    @NotNull(message = "La fecha de nacimineto es un campo requerido")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fechaNacimiento;


    public static StudentDTO fromDomain(Student student) {
        return StudentDTO.builder()
                .id(student.getId())
                .nombre(student.getNombre())
                .apellido(student.getApellido())
                .email(student.getEmail())
                .apellido(student.getApellido())
                .dni(student.getDni())
                .edad(student.getEdad())
                .fechaNacimiento(student.getFechaNacimiento())
                .build();
    }

    public static Student toDomain(StudentDTO studentDTO) {
        return Student.builder()
                .id(studentDTO.getId())
                .nombre(studentDTO.getNombre())
                .apellido(studentDTO.getApellido())
                .email(studentDTO.getEmail())
                .dni(studentDTO.getDni())
                .edad(studentDTO.getEdad())
                .fechaNacimiento(studentDTO.getFechaNacimiento())
                .build();
    }
}
