package com.example.demo.application.port.out;

import com.example.demo.domain.Student;

import java.util.List;
import java.util.Optional;

public interface StudentPort {

    Optional<Student> find(Long id);
    Student save(Student student);
    List<Student> findAll();
    void delete(Long id);
    Student update(Long id, Student student);
}
