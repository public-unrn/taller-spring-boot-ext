package com.example.demo.application.service.estudiante;

import com.example.demo.application.port.in.SaveStudentUseCase;
import com.example.demo.application.port.out.StudentPort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Student;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class SaveStudentService implements SaveStudentUseCase {
    private final StudentPort studentPort;

    @Override
    public Student save(Student student) {
        Student result = studentPort.save(student);
        return result;
    }
}
