package com.example.demo.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Student {
    private Long id;
    private String nombre;
    private String apellido;
    private String email;
    private String dni;
    private int edad;
    private LocalDate fechaNacimiento;

    public boolean esMayor() {
        return this.edad >= 18;
    }
}
