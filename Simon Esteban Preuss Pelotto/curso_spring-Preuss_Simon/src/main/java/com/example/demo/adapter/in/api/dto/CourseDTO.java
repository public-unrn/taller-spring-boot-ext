package com.example.demo.adapter.in.api.dto;

import com.example.demo.domain.Course;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CourseDTO {

    private Long id;

    @NotBlank(message = "El nombre no puede estar vacío")
    private String nombre;

    @NotBlank(message = "La descripcion no puede estar vacía")
    private String descipcion;

    @NotNull(message = "La fecha de creacion es un campo requerido")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fechaInicio;

    @NotNull(message = "La fecha de finalizacion es un campo requerido")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate fechaFin;

    public static CourseDTO fromDomain(Course course) {
        return CourseDTO.builder()
                .id(course.getId())
                .nombre(course.getNombre())
                .descipcion(course.getDescipcion())
                .fechaInicio(course.getFechaInicio())
                .fechaFin(course.getFechaFin())
                .build();
    }

    public static Course toDomain(CourseDTO curso) {
        return Course.builder()
                .id(curso.getId())
                .nombre(curso.getNombre())
                .descipcion(curso.getDescipcion())
                .fechaInicio(curso.getFechaInicio())
                .fechaFin(curso.getFechaFin())
                .build();
    }
}
