package com.example.demo.adapter.in.api.dto;

import com.example.demo.domain.Course;
import com.example.demo.domain.State;
import com.example.demo.domain.Student;
import com.example.demo.domain.Inscription;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
public class InscriptionDTO {
    private Long id;

    @NotNull(message = "La fecha de inscripcion es un campo requerido")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime fechaInscripcion;

    @NotNull(message = "El estado es un campo requerido")
    private State state;

    @NotNull(message = "El id del curso es un campo requerido")
    private Long curso;

    @NotNull(message = "El id del estudiante es un campo requerido")
    private Long estudiante;

    public static Inscription toDomain(InscriptionDTO inscriptionDTO) {
        return Inscription.builder()
                .id(inscriptionDTO.getId())
                .fechaInscripcion(inscriptionDTO.getFechaInscripcion())
                .state(inscriptionDTO.getState())
                .course(Course.builder()
                        .id(inscriptionDTO.getCurso())
                        .build())
                .student(Student.builder()
                        .id(inscriptionDTO.getEstudiante())
                        .build())
                .build();
    }

    public static InscriptionDTO fromDomain(Inscription inscription) {
        return InscriptionDTO.builder()
                .id(inscription.getId())
                .fechaInscripcion(inscription.getFechaInscripcion())
                .state(inscription.getState())
                .curso(inscription.getCourse().getId())
                .estudiante(inscription.getStudent().getId())
                .build();
    }
}
