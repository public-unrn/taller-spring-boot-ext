package com.example.demo.application.port.in;

import com.example.demo.domain.Student;

public interface FindStudentUseCase {

    Student find(Long id);
}
