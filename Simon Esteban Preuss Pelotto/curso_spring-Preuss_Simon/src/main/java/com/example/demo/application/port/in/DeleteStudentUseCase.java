package com.example.demo.application.port.in;

public interface DeleteStudentUseCase {
    void delete(Long id);
}
