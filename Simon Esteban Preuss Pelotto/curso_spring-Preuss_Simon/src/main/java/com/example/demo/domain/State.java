package com.example.demo.domain;

public enum State {
    ACEPTADA, RECHAZADA, PENDIENTE
}
