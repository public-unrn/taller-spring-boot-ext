package com.example.demo.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class Course {
    private Long id;
    private String nombre;
    private String descipcion;
    private LocalDate fechaInicio;
    private LocalDate fechaFin;
}
