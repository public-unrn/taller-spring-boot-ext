package com.example.demo.application.port.in;

import com.example.demo.domain.Student;

public interface UpdateStudentUseCase {
    Student update(Long id, Student student);
}
