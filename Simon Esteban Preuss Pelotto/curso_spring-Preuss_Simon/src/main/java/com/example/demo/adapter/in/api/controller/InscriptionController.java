package com.example.demo.adapter.in.api.controller;

import com.example.demo.adapter.in.api.dto.InscriptionDTO;
import com.example.demo.application.port.in.SaveInscriptionUseCase;
import com.example.demo.domain.Inscription;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Tag(name = "Inscripcion")
@RequestMapping("/inscripcion")
public class InscriptionController {

    private final SaveInscriptionUseCase saveInscriptionUseCase;

    @Operation(summary = "Guardar una inscripcion")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Operacion exitosa", content = {
                    @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = InscriptionDTO.class))
                    )
            }),
            @ApiResponse(responseCode = "400", description = "Bad request", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE
            )),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(
                    mediaType = MediaType.APPLICATION_JSON_VALUE
            ))
    })
    @PostMapping
    ResponseEntity<InscriptionDTO> save(@Valid @org.springframework.web.bind.annotation.RequestBody InscriptionDTO inscriptionDTO) {
        Inscription response = saveInscriptionUseCase.save(InscriptionDTO.toDomain(inscriptionDTO));

        return new ResponseEntity<>(InscriptionDTO.fromDomain(response), HttpStatus.OK);
    }
}
