package com.example.demo.application.service.curso;

import com.example.demo.application.port.in.SaveCourseUseCase;
import com.example.demo.application.port.out.CoursePort;
import com.example.demo.common.UseCase;
import com.example.demo.domain.Course;
import lombok.RequiredArgsConstructor;

@UseCase
@RequiredArgsConstructor
public class SaveCourseService implements SaveCourseUseCase {

    private final CoursePort coursePort;

    @Override
    public Course save(Course course) {
        Course result = coursePort.save(course);
        return result;
    }
}
