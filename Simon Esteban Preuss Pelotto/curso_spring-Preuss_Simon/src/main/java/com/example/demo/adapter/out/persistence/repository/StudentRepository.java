package com.example.demo.adapter.out.persistence.repository;

import com.example.demo.adapter.out.persistence.entity.StudentEntity;
import com.example.demo.domain.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
    List<Student> findByDniGreaterThanAndApellido(String dni, String apellido);
    Page<Student> findAllByOrderByDniAsc(Pageable pageable);
}
