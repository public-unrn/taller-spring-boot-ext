package com.example.demo.application.port.in;

import com.example.demo.domain.Student;

public interface SaveStudentUseCase {

    Student save (Student student);
}
