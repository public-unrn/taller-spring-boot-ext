package com.example.demo.application.port.out;

import com.example.demo.domain.Course;

public interface CoursePort {
    Course save(Course course);
}
