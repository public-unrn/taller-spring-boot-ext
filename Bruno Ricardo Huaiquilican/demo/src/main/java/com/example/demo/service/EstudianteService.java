package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad(),
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido());
        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> consultameTodosLosEstudiantes() {
        return estudianteRepository.findAll().stream().map(e -> new EstudianteDTO(
                e.getDni(), e.getEmail(),
                e.getFechaDeNacimiento(),
                e.getEdad(), e.getNombre(),
                e.getApellido())).collect(Collectors.toList());
    }

    public EstudianteDTO actualizarEstudiate(Long id, EstudianteDTO estudianteDTO) {
        Estudiante estudiante = new Estudiante(
                id,
                estudianteDTO.getDni(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento(),
                estudianteDTO.getEdad(),
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido());
        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public EstudianteDTO consultarPorId(Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isEmpty()) {
            throw new RuntimeException("El Id no es valido");
        }
        Estudiante estudiante = estudianteOptional.get();

        return new EstudianteDTO(estudiante.getDni(), estudiante.getEmail(),
                estudiante.getFechaDeNacimiento(), estudiante.getEdad(),
                estudiante.getNombre(), estudiante.getApellido());
    }

    public void eliminar(Long id) {
        estudianteRepository.deleteById(id);
    }
}