package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "Curso")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Curso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column(name = "Descripcion")
    private String descripcion;

    @Column(name = "Fecha_De_Inicio")
    private LocalDate fechaDeInicio;

    @Column(name = "Fecha_De_Fin")
    private LocalDate fechaDeFin ;

    @Column(name = "Nombre")
    private String nombre;
}
