package com.example.demo;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import com.example.demo.service.InscripcionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;

	@Autowired
	EstudianteRepository estudianteRepository;

	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionesService InscripcionesService;
	private void saveCursos() {

		cursoRepository.saveAll(Arrays.asList(
				new Curso(null, "Crack ", LocalDate.of(2020, 5, 20), LocalDate.of(2022, 4, 20), "Curso A"),
				new Curso(null, "Crack ", LocalDate.of(2020, 5, 20), LocalDate.of(2022, 4, 20), "Curso B"),
				new Curso(null, "Crack ", LocalDate.of(2020, 5, 20), LocalDate.of(2022, 4, 20), "Curso C"),
				new Curso(null, "Crack ", LocalDate.of(2020, 5, 20), LocalDate.of(2022, 4, 20), "Curso D")
		));
	}

	private void saveEsudiante() {
		estudianteRepository.saveAll(Arrays.asList(
				new Estudiante(null, 40707082, "Bruno@huaiqui.com", LocalDate.of(2002, 5, 13), 21, "Bruno", "Huaiquilican"),
				new Estudiante(null, 20707082, "Ricardo@huaiqui.com", LocalDate.of(2000, 3, 13), 18, "Ricardo", "Huaiquilican"),
				new Estudiante(null, 30707092, "Bruno@huaiqui.com", LocalDate.of(2005, 5, 13), 21, "bochini", "Rome"),
				new Estudiante(null, 10707092, "Bruno@huaiqui.com", LocalDate.of(2010, 7, 19), 21, "Ale", "Huaiquilican")
		));
	}
	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args ->{
			saveCursos();
			saveEsudiante();

			InscripcionesService.inscripciones(1L, 1L, LocalDate.of(2020, 2, 13), Estado.RECHAZADA);
			InscripcionesService.inscripciones(2L, 2L, LocalDate.of(2023, 7, 3), Estado.PENDIENTE);
			InscripcionesService.inscripciones(3L, 3L, LocalDate.of(2022, 6, 23), Estado.ACEPTADA);
			InscripcionesService.inscripciones(4L, 3L, LocalDate.of(2015, 1, 13), Estado.RECHAZADA);
		};
	}


}