package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

    @Query("SELECT e FROM Estudiante e ")
    List<Estudiante> buscarEstudiante();

    @Query("SELECT e FROM Estudiante e WHERE e.dni > 20000000 AND e.apellido = 'Romero'")
    List<Estudiante> buscarPorDniApellido();

    List<Estudiante> findBy();

    List<Estudiante> findByDniOrApellido(int dni, String apellido);

    @Query("SELECT e FROM Estudiante e ")
    Page<Estudiante> bucarTodoPaginando(Pageable pageable);
}
