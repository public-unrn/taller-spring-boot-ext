package com.example.demo.controller;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    @PostMapping
    public EstudianteDTO estudianteNuevo(@RequestBody EstudianteDTO estudianteDTO)
    {
        return estudianteService.saveEstudiante(estudianteDTO);
    }

    @GetMapping
    public List<EstudianteDTO> mostrarEstudiantes()
    {
        return estudianteService.consultameTodosLosEstudiantes();
    }

    @GetMapping("/{id}")
    public EstudianteDTO consultaEstudiantePorId(@PathVariable Long id)
    {
        return estudianteService.consultarPorId(id);
    }
    @PutMapping("/{id}")
    public EstudianteDTO actualizaEstudiante(@PathVariable Long id,@RequestBody EstudianteDTO userDTO)
    {
        return estudianteService.actualizarEstudiate(id, userDTO);
    }
    @DeleteMapping("/{id}")
    public void eliminarPorId(@PathVariable Long id)
    {
        estudianteService.eliminar(id);
    }

}
