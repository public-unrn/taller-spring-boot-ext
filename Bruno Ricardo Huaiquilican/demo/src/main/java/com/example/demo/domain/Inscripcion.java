package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "Inscripcion")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id ;

    @Column(name = "Estado")
    @Enumerated(EnumType.STRING)
    private Estado estado;

    @ManyToOne
    private Curso curso;

    @Column(name = "Fecha_de_Incripcion")
    private LocalDate fechaDeIncripcion ;

    @ManyToOne
    private Estudiante estudiante;

}
