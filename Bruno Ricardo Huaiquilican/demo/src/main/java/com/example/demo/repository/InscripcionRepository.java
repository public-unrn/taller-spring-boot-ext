package com.example.demo.repository;


import com.example.demo.domain.Estado;
import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface InscripcionRepository extends JpaRepository <Inscripcion, Long> {

    @Query("SELECT i FROM Inscripcion i WHERE i.estado = 'RECHAZADA' OR i.estado = 'PENDIENTE'")
    List<Inscripcion> bucarIncripciones();

    List<Inscripcion>findByEstado(Estado estado);


    @Query(value = "SELECT * FROM incripcion i WHERE i.estado = 'PENDIENTE'", nativeQuery = true)
    List<Inscripcion> bucarEstadoNativ();
}
