package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "Estudiante")
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Estudiante {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Dni")
    private int dni;

    @Column(name = "Email")
    private String email;

    @Column(name = "Fecha_De_Nacimiento")
    private LocalDate fechaDeNacimiento;

    @Column(name = "Edad")
    private int edad;

    @Column(name = "Nombre")
    private String nombre;

    @Column(name = "Apellido")
    private String apellido;


    public boolean esMayorEdad()
    {
        return edad >= 18 ;
    }
}