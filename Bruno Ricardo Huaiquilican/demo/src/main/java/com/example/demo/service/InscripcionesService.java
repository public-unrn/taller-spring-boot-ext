package com.example.demo.service;
import com.example.demo.domain.Curso;
import com.example.demo.domain.Estado;
import com.example.demo.domain.Estudiante;
import com.example.demo.domain.Inscripcion;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import java.time.LocalDate;
@Service
@Validated
public class InscripcionesService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

    @Transient
    public void inscripciones(@NotNull @Positive Long estudianteId , @NotNull @Positive Long cursoId , LocalDate fechaDeIncripcion , Estado estado ){

       Estudiante estudiante = estudianteRepository.findById(estudianteId).orElseThrow(()-> new RuntimeException("El id de customer no es valido"));

           if(!estudiante.esMayorEdad()) {
         throw new RuntimeException("El estudiante no es mayor de edad...");
           }


       Curso curso = cursoRepository.findById(cursoId).orElseThrow(
               ()-> new RuntimeException("El Id del curso no es valido"));

       Inscripcion inscripcion = new Inscripcion( null , estado , curso , fechaDeIncripcion ,estudiante);

        inscripcionRepository.save(inscripcion);
    }
}
