package com.example.demo.controller;
import com.example.demo.dto.InscribcionDTO;
import com.example.demo.service.InscripcionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/incripcion")
public class InscripcioneController {

    @Autowired
    private InscripcionesService inscripcionesService;

    @PostMapping
    public void nuevaIncripcion(@RequestBody InscribcionDTO inscribcionDTO)
    {
        inscripcionesService.inscripciones(
                inscribcionDTO.getEstudiante(),
                inscribcionDTO.getCurso(),
                inscribcionDTO.getFechaDeIncripcion(),
                inscribcionDTO.getEstado());
    }
}
