package com.example.demospring.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "estudiante")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private String apellido;

    private String email;

    private String dni;

    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    public boolean esMayorEdad() {
        return fechaNacimiento.plusYears(18).isBefore(LocalDate.now());
    }
}
