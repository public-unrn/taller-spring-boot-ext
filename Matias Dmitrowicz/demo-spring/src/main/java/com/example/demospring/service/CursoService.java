package com.example.demospring.service;

import com.example.demospring.domain.Curso;
import com.example.demospring.dto.CursoDTO;
import com.example.demospring.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {

    @Autowired
    CursoRepository cursoRepository;
    public CursoDTO saveCurso(CursoDTO cursoDTO) {
        Curso curso = new Curso(null,
                cursoDTO.getNombre(),
                cursoDTO.getDescripcion(),
                cursoDTO.getFechaInicio(),
                cursoDTO.getFechaFin());
        cursoRepository.save(curso);
        return cursoDTO;
    }
}
