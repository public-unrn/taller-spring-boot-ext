package com.example.demospring.domain;

public enum EstadoInscripcion {
    ACEPTADA, RECHAZADA, PENDIENTE
}