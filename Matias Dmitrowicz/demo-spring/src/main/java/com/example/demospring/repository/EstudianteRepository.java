package com.example.demospring.repository;

import com.example.demospring.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante, Long> {

//    B. Listar todos los estudiantes
    @Query("SELECT e FROM Estudiante e")
    List<Estudiante> listarEstudiantes();

//    C. Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea
//    “Romero”
    @Query("SELECT e FROM Estudiante e WHERE e.dni>'20' AND e.apellido='Romero'")
    List<Estudiante> listarEstudiantesDniMayor20AndApellidoRomero();


//    E. Consulta derivada del punto #B y #C
//    Listar estudiantes --> findAll() #metodo incorporado
    List<Estudiante> findAllByDniAfterAndApellido(String dni, String apellido);

//    I. Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
//    Probar las siguientes combinaciones:
//        a. pagina 1, tamaño 5 // pagina 1 con 5 estudiantes
//        b. pagina 0, tamaño 2 // pagina 0 con 2 estudiantes
    Page<Estudiante> findAllByOrderByDniAsc(Pageable pageable);
    // estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(1,5));
    // estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(0,2))

}
