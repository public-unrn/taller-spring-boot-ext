package com.example.demospring.dto;

import com.example.demospring.domain.Curso;
import com.example.demospring.domain.EstadoInscripcion;
import com.example.demospring.domain.Estudiante;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InscripcionDTO {
    private LocalDate fechaInscripcion;
    private String estado;
    private Long curso;
    private Long estudiante;
}
