package com.example.demospring.service;

import com.example.demospring.domain.Estudiante;
import com.example.demospring.dto.EstudianteDTO;
import com.example.demospring.repository.EstudianteRepository;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jakarta.validation.constraints.Positive;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    public EstudianteDTO saveEstudiante(EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getEmail(),
                estudianteDTO.getDni(),
                estudianteDTO.getFechaNacimiento());

        estudianteRepository.save(estudiante);

        return estudianteDTO;
    }

    public List<EstudianteDTO> findAll(){
        return estudianteRepository.findAll()
                .stream().map(e -> new EstudianteDTO(e.getNombre(), e.getApellido(), e.getEmail(), e.getDni(),
                        e.getFechaNacimiento())).collect(Collectors.toList());
    }

    public EstudianteDTO find(@NotNull @Positive Long id) {
        Optional<Estudiante> estudianteOptional = estudianteRepository.findById(id);

        if (estudianteOptional.isPresent()) {
            // Si el estudiante existe lo creo para retornarlo en DTO como respuesta
            Estudiante estudiante = estudianteOptional.get();
            EstudianteDTO estudianteDTO = new EstudianteDTO();
            estudianteDTO.setNombre(estudiante.getNombre());
            estudianteDTO.setApellido(estudiante.getApellido());
            estudianteDTO.setEmail(estudiante.getEmail());
            estudianteDTO.setDni(estudiante.getDni());
            estudianteDTO.setFechaNacimiento(estudiante.getFechaNacimiento());

            return estudianteDTO;
        } else {
            throw new RuntimeException("Estudiante no encontrado con ID: " + id);
        }
    }

    public EstudianteDTO update(@NotNull @Positive Long id, EstudianteDTO estudianteDTO) {
        // Buscar el estudiante por su ID
        Estudiante estudianteExistente = estudianteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Estudiante no encontrado con ID: " + id));

        // Actualizar los campos del estudiante existente con los valores nuevos del DTO
        estudianteExistente.setNombre(estudianteDTO.getNombre());
        estudianteExistente.setApellido(estudianteDTO.getApellido());
        estudianteExistente.setEmail(estudianteDTO.getEmail());
        estudianteExistente.setDni(estudianteDTO.getDni());
        estudianteExistente.setFechaNacimiento(estudianteDTO.getFechaNacimiento());

        // Guardar el estudiante actualizado en la base de datos
        Estudiante estudianteActualizado = estudianteRepository.save(estudianteExistente);

        // Convertir el estudiante actualizado en un DTO para retornarlo como respuesta
        EstudianteDTO estudianteDTOActualizado = new EstudianteDTO();
        estudianteDTOActualizado.setNombre(estudianteActualizado.getNombre());
        estudianteDTOActualizado.setApellido(estudianteActualizado.getApellido());
        estudianteDTOActualizado.setEmail(estudianteActualizado.getEmail());
        estudianteDTOActualizado.setDni(estudianteActualizado.getDni());
        estudianteDTOActualizado.setFechaNacimiento(estudianteActualizado.getFechaNacimiento());

        return estudianteDTOActualizado;

    }

    public void delete(@NotNull @Positive Long id) {
        // Buscar el estudiante por su ID
        Estudiante estudiante = estudianteRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Estudiante no encontrado con ID: " + id));
        estudianteRepository.delete(estudiante);
    }
}
