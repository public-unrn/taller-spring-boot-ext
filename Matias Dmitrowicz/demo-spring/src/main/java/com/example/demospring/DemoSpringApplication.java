package com.example.demospring;

import com.example.demospring.domain.Curso;
import com.example.demospring.domain.EstadoInscripcion;
import com.example.demospring.domain.Estudiante;
import com.example.demospring.domain.Inscripcion;
import com.example.demospring.repository.CursoRepository;
import com.example.demospring.repository.EstudianteRepository;
import com.example.demospring.repository.InscripcionRepository;
import com.example.demospring.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootApplication
public class DemoSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringApplication.class, args);
	}

	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	@Autowired
	InscripcionService inscripcionService;

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx){
		return args -> {

			Curso c1 = new Curso(null,"BD2", "Bases de Datos 2", LocalDate.of(2020, 1, 1),LocalDate.of(2020, 6, 30));
			Curso c2 = new Curso(null,"Ing 3", "Ingeniería de Software 3", LocalDate.of(2020, 6, 1),LocalDate.of(2020, 11, 30));
			cursoRepository.save(c1);
			cursoRepository.save(c2);

			cursoRepository.findAll();

			Estudiante e1 = new Estudiante(null,"Juan","Perez","jp@gmail.com","10",LocalDate.of(2000, 5, 10));
			Estudiante e2 = new Estudiante(null,"Pedro","Romero","pp@gmail.com","25",LocalDate.of(2000, 8, 17));
			estudianteRepository.save(e1);
			estudianteRepository.save(e2);

			estudianteRepository.findAll();

			inscripcionService.inscribirEstudiante(2L,2L,LocalDate.of(2023,2,26),EstadoInscripcion.PENDIENTE);

			inscripcionRepository.findAll();
			inscripcionRepository.findById(1L);
		};
	}
}
