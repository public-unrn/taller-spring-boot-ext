package com.example.demospring.repository;

import com.example.demospring.domain.Curso;
import com.example.demospring.domain.EstadoInscripcion;
import com.example.demospring.domain.Estudiante;
import com.example.demospring.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion, Long> {

//    D. Listar todas las inscripciones rechazadas o pendiente
    @Query("SELECT i FROM Inscripcion i where i.estado='RECHAZADA' or i.estado='PENDIENTE'")
    List<Inscripcion> listarInscripcionesRechazadasOrPendientes();

    //    E. Consulta derivada del punto #D
    // No queda igual al tener que pasarle los dos estados, pero no se me ocurrio otra forma
    List<Inscripcion> findByEstadoOrEstado(EstadoInscripcion estadoRechazada, EstadoInscripcion estadoPendiente);

//    G. Listar todas las inscripciones en base a un parámetro de estado
    @Query("SELECT i FROM Inscripcion i where i.estado = :estado")
    List<Inscripcion> listarInscripcionesByEstado(@Param("estado") EstadoInscripcion estado);

//    H. Listar todas las inscripciones en base a un parámetro de estado utilizando consulta nativa
    @Query(value = "SELECT * FROM inscripcion WHERE estado = :estado", nativeQuery = true)
    List<Inscripcion> findInscripcionesPorEstado(@Param("estado") String estado);




}

