package com.example.demospring.repository;

import com.example.demospring.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CursoRepository extends JpaRepository<Curso, Long> {

    // A. Listar todos los cursos
    @Query("SELECT c FROM Curso c")
    List<Curso> listarCursos();

//    E. Consulta derivada del punto #A
//    Listar cursos --> findAll() #metodo incorporado

//    F. Listar todos los cursos que hayan empezado después de “01/02/2020”
    @Query(value = "SELECT * FROM curso c WHERE c.fecha_inicio > '2020-02-01'", nativeQuery = true)
    List<Curso> listarCursosComenzadosDespuesDe2020Feb01();

}

