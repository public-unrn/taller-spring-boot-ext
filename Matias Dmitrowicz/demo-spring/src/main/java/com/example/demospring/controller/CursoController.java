package com.example.demospring.controller;

import com.example.demospring.dto.CursoDTO;
import com.example.demospring.dto.EstudianteDTO;
import com.example.demospring.repository.CursoRepository;
import com.example.demospring.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/curso")
public class CursoController {

    @Autowired
    private CursoService cursoService;

//    Creación de un curso
    @PostMapping
    public CursoDTO save(@RequestBody CursoDTO cursoDTO){

        return cursoService.saveCurso(cursoDTO);
    }

}
