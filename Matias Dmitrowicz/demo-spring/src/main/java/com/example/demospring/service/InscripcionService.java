package com.example.demospring.service;

import com.example.demospring.domain.Curso;
import com.example.demospring.domain.EstadoInscripcion;
import com.example.demospring.domain.Estudiante;
import com.example.demospring.domain.Inscripcion;
import com.example.demospring.dto.InscripcionDTO;
import com.example.demospring.repository.CursoRepository;
import com.example.demospring.repository.EstudianteRepository;
import com.example.demospring.repository.InscripcionRepository;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class InscripcionService {

    @Autowired
    private CursoRepository cursoRepository;

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Autowired
    private InscripcionRepository inscripcionRepository;

//  Registrar una inscripción donde se tiene que indicar el estudiante y curso.
    public void inscribirEstudiante(@NotNull @Positive Long estudianteId, @NotNull @Positive Long cursoId, LocalDate fechaInscripcion, EstadoInscripcion estadoInscripcion) {
        // Buscar el estudiante y el curso por sus respectivos ID's, si no existen, notificar error
        Estudiante estudiante = estudianteRepository.findById(estudianteId)
                .orElseThrow(() -> new IllegalArgumentException("Estudiante no encontrado con ID: " + estudianteId));
        Curso curso = cursoRepository.findById(cursoId)
                .orElseThrow(() -> new IllegalArgumentException("Curso no encontrado con ID:" + cursoId));

        // Verificar que el estudiante sea mayor de edad
        if (!estudiante.esMayorEdad()) {
            throw new IllegalArgumentException("El estudiante debe ser mayor de edad para inscribirse.");
        }

        // Crear una nueva inscripción
        Inscripcion inscripcion = new Inscripcion(
                null,
                fechaInscripcion,
                estadoInscripcion,
                curso,
                estudiante
        );

        // Guardar la inscripción
        inscripcionRepository.save(inscripcion);
    }

    public InscripcionDTO saveInscripcion(InscripcionDTO inscripcionDTO) {

        // Buscar el estudiante y el curso por sus respectivos ID's, si no existen, notificar error
        Estudiante estudiante = estudianteRepository.findById(inscripcionDTO.getEstudiante())
                .orElseThrow(() -> new IllegalArgumentException("Estudiante no encontrado con ID: " + inscripcionDTO.getEstudiante()));
        Curso curso = cursoRepository.findById(inscripcionDTO.getCurso())
                .orElseThrow(() -> new IllegalArgumentException("Curso no encontrado con ID:" + inscripcionDTO.getCurso()));

        // Verificar que el estudiante sea mayor de edad
        if (!estudiante.esMayorEdad()) {
            throw new IllegalArgumentException("El estudiante debe ser mayor de edad para inscribirse.");
        }

        // Verificar que la fecha de inscripción sea posterior que la fecha de inicio del curso
        if (inscripcionDTO.getFechaInscripcion().isBefore(curso.getFechaInicio())) {
            throw new IllegalArgumentException("La fecha de inscripción debe ser posterior a la fecha de inicio del curso.");
        }

        // Verificar que la fecha de inscripción sea anterior que la fecha de fin del curso
        if (inscripcionDTO.getFechaInscripcion().isAfter(curso.getFechaFin())) {
            throw new IllegalArgumentException("La fecha de inscripción debe ser anterior a la fecha de fin del curso.");
        }
        // Convertir el string del estado a un objeto EstadoInscripcion
        EstadoInscripcion estado = EstadoInscripcion.valueOf(inscripcionDTO.getEstado());

        Inscripcion inscripcion = new Inscripcion(null,
                inscripcionDTO.getFechaInscripcion(), estado, curso, estudiante);

        inscripcionRepository.save(inscripcion);
        return inscripcionDTO;
    }
}
