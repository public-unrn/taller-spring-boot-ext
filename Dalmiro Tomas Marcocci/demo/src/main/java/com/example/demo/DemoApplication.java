package com.example.demo;


import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.util.Arrays;


@SpringBootApplication
public class DemoApplication {

	@Autowired
	private CursoRepository cursoRepository;
	@Autowired
	private EstudianteRepository estudianteRepository;
	@Autowired
	private InscripcionRepository inscripcionRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner commandLineRunner (ApplicationContext ctx){
		return args -> {
	/*		//Lista todos los cursos
			cursoRepository.findAll();
			//Lista todos los estudiantes
			estudianteRepository.findAll();
			//Listar todos los estudiantes que tengan un dni mayor a 20M y que su apellido sea “Romero”
			estudianteRepository.findAllByDniGreaterThanAndApellido(20000000,"Romero");
			//Listar todas las inscripciones rechazadas o pendiente
			inscripcionRepository.findAllByEstadoIn(Arrays.asList("rechazadas","pendiente"));
			//Listar todos los cursos que hayan empezado después de “01/02/2020”
			//cursoRepository.findAllByFechaInicioAfter(LocalDate.parse("01/02/2020"));
			// Listar todas las inscripciones en base a un parámetro de estado
			inscripcionRepository.findAllByEstado("AlgunEstado");
			//H. Listar todas las inscripciones en base a un parámetro de estado utilizando consulta Nativa
			inscripcionRepository.findAllByEstadoNativeQuery("AlgunEstado");

			//Listar todos los estudiantes de forma paginada y ordenada ascendente por DNI
			//pagina 1, tamaño 5
			estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(1, 5, Sort.by("dni").ascending())).getContent();
			//pagina 0, tamaño 2
			//estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(0, 2, Sort.by("dni").ascending())).getContent();

	*/


		};
	}

}
