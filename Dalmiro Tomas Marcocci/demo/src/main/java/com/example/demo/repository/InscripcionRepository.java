package com.example.demo.repository;

import com.example.demo.domain.Inscripcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository

public interface InscripcionRepository extends JpaRepository<Inscripcion,Long> {

    @Query("Select i from inscripciones i where i.estado='rechazadas' or i.estado'pendiente'")
    List<Inscripcion> findAllByEstadoInQuery();

    List<Inscripcion> findAllByEstadoIn(List<String> estado);

    List<Inscripcion> findAllByEstado(String estado);

    @Query(value = "Select * from inscripcion where estado =:estado",nativeQuery = true)
    List<Inscripcion> findAllByEstadoNativeQuery(@Param("estado") String estado);
}
