package com.example.demo.service;

import com.example.demo.domain.Estudiante;
import com.example.demo.dto.EstudianteDTO;
import com.example.demo.repository.EstudianteRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Validated
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Transactional
    public EstudianteDTO save(@NonNull EstudianteDTO estudianteDTO){

        Estudiante estudiante = new Estudiante(
                null,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getDni(),
                estudianteDTO.getEdad(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento()
        );
    estudianteRepository.save(estudiante);
    return estudianteDTO;

    }

    public List<EstudianteDTO>findAll(){
        return estudianteRepository.findAll()
                .stream().map(c->new EstudianteDTO(c.getNombre(), c.getApellido(), c.getDni(),c.getEdad(),c.getEmail(),c.getFechaDeNacimiento()))
                .collect(Collectors.toList());
    }

    public EstudianteDTO findById(@NonNull @Positive Long id){
        var estudiante = estudianteRepository.findById(id).orElseThrow();
        return new EstudianteDTO(estudiante.getNombre(),estudiante.getApellido(),estudiante.getDni(),estudiante.getEdad(),estudiante.getEmail(),estudiante.getFechaDeNacimiento());
    }

    public EstudianteDTO update(@NonNull @Positive Long id,@NonNull  EstudianteDTO estudianteDTO){
        Estudiante estudiante = new Estudiante(
                id,
                estudianteDTO.getNombre(),
                estudianteDTO.getApellido(),
                estudianteDTO.getDni(),
                estudianteDTO.getEdad(),
                estudianteDTO.getEmail(),
                estudianteDTO.getFechaDeNacimiento()
        );
        estudianteRepository.save(estudiante);
        return estudianteDTO;
    }

    public void delete(@NonNull @Positive Long id){
        estudianteRepository.deleteById(id);
    }



}
