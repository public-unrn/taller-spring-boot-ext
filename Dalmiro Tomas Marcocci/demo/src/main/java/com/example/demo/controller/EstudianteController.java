package com.example.demo.controller;

import com.example.demo.dto.EstudianteDTO;
import com.example.demo.service.EstudianteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiante")
public class EstudianteController {

    @Autowired
    private EstudianteService estudianteService;

    //Creación de estudiante
    @PostMapping
    public EstudianteDTO save(@RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.save(estudianteDTO);
    }
    //Consulta de todos los estudiante
    @GetMapping
    public List<EstudianteDTO> findAll(){
        return estudianteService.findAll();
    }

    //Consulta de un estudiante en particular por id
    @GetMapping("/{id}")
    public EstudianteDTO find(@PathVariable Long id){
        return estudianteService.findById(id);
    }

    //Actualización de un estudiante
    @PutMapping("/{id}")
    public EstudianteDTO update(@PathVariable Long id, @RequestBody EstudianteDTO estudianteDTO){
        return estudianteService.update(id,estudianteDTO);
    }

    //Eliminación de un estudiante
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id){
    estudianteService.delete(id);
    }

}
