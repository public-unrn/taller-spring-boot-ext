package com.example.demo.repository;

import com.example.demo.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface EstudianteRepository extends JpaRepository<Estudiante,Long> {

    @Query("Select e from estudiantes e")
    List<Estudiante> findAllQuery();

    //Lista a todos los estudiantes
    List<Estudiante> findAll();

    @Query("Select e from estudiantes e where e.dni>20000000 and e.apellido='Romero'")
    List<Estudiante> findAllByDniGreaterThanAndApellidoQuery();

    //Lista a todos los estudiantes que tengan dni mayor y el apellido sea el pasado por parametro
    List<Estudiante> findAllByDniGreaterThanAndApellido(int dni , String apellido);

    //Lista todos los estudiantes de forma paginada y ordenada ascendente por DNI
    Page<Estudiante> findAllByOrderByDniAsc(Pageable pageable);



}
