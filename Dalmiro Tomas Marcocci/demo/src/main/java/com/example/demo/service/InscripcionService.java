package com.example.demo.service;

import com.example.demo.domain.Inscripcion;
import com.example.demo.dto.InscripcionDTO;
import com.example.demo.repository.CursoRepository;
import com.example.demo.repository.EstudianteRepository;
import com.example.demo.repository.InscripcionRepository;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import java.time.LocalDate;

@Service
@AllArgsConstructor
@Validated
public class InscripcionService {

    @Autowired
    private InscripcionRepository inscripcionRepository;
    @Autowired
    private EstudianteRepository estudianteRepository;
    @Autowired
    private CursoRepository cursoRepository;

    @Transactional
    public InscripcionDTO save(@NonNull @Positive Long cursoId, @NonNull @Positive Long estudianteId,String estado,LocalDate fechaInscripcion){

        var curso = cursoRepository.findById(cursoId).orElseThrow();

        var estudiante = estudianteRepository.findById(estudianteId).orElseThrow();

        if(estudiante.esMenor()){
            throw new RuntimeException("No es posible registrar la Inscripcion porque el estudiante es menor de edad");
        }
        if(!curso.estaVigente()){
            throw new RuntimeException("No se puede realizar la inscripcion porque el curso ya finalizo");
        }
        Inscripcion inscripcion = new Inscripcion(null,estado,curso, fechaInscripcion,estudiante);
        inscripcionRepository.save(inscripcion);
        return new InscripcionDTO(estado,cursoId,fechaInscripcion,estudianteId);
    }
}
