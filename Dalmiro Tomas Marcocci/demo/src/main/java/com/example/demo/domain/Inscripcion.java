package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "inscripcion")
@AllArgsConstructor
@NoArgsConstructor
public class Inscripcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "estado")
    private String estado;
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "curso_id")
    private Curso curso;
    @Column(name = "fecha_inscripcion")
    private LocalDate fechaInscripcion;
    @ManyToOne (cascade = CascadeType.ALL)
    @JoinColumn(name = "estudiante_id")
    private Estudiante estudiante;
}
