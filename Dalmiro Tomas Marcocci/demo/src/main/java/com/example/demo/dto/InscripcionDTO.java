package com.example.demo.dto;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDTO {

    private String estado;
    private Long curso;
    private LocalDate fechaInscripcion;
    private Long estudiante;
}
