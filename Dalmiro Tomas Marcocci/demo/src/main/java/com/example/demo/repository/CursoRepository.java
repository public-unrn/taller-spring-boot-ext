package com.example.demo.repository;

import com.example.demo.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CursoRepository extends JpaRepository<Curso,Long> {



    @Query("Select c FROM curso c")
    List <Curso> findAllQuery();



    //Lista todos los cursos consultas derivadas
    List <Curso> findAll();

    //Lista los todos los cursos despues de un a determinada fecha pasada por parametro consultas derivadas
    List<Curso> findAllByFechaInicioAfter(LocalDate fechaLimite);


}
