package com.example.demo.domain;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name ="estudiante")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apellido")
    private String apellido;
    @Column(name = "dni")
    private int dni;
    @Column(name = "edad")
    private int edad;
    @Column(name = "email")
    private String email;
    @Column(name = "fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;

    private static final int MENOR_DE_EDAD = 18;


    public boolean esMenor(){
        return MENOR_DE_EDAD>edad;
    }

}
