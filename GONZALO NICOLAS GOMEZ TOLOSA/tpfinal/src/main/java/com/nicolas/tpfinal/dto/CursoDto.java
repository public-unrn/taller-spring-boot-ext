package com.nicolas.tpfinal.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CursoDto {

    private String nombre;
    private String descipcion;
    private LocalDate fechaDeInicio;
    private LocalDate fechaDeFin;
}
