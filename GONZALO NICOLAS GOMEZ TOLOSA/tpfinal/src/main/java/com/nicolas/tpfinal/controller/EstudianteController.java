package com.nicolas.tpfinal.controller;

import com.nicolas.tpfinal.dto.EstudianteDto;
import com.nicolas.tpfinal.service.EstudianteService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/estudiantes")
public class EstudianteController {
@Autowired
    EstudianteService estudianteService;
    @PostMapping
    public void crearEstudiante(@RequestBody EstudianteDto estudianteDto){
        estudianteService.create(estudianteDto);
    }
    @GetMapping
    public List<EstudianteDto> listarEstudiantes(){
        return estudianteService.findAll();
    }
    @GetMapping("/{id}")
    public EstudianteDto buscarEstudiante(@RequestParam Long id){
        return estudianteService.find(id);
    }
    @PutMapping("/{id}")
    public void modificarEstudiante(@RequestParam Long id,@RequestBody EstudianteDto estudianteDto){
        estudianteService.update(id,estudianteDto);
    }
    @DeleteMapping("/{id}")
    public void eliminarEstudiante(@RequestParam Long id){
        estudianteService.delete(id);
    }
}
