package com.nicolas.tpfinal.domain;

import jakarta.persistence.*;
import lombok.*;


import java.time.LocalDate;
@Entity
@Table(name="cursos")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Curso {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="nombre")
    private String nombre;

    @Column(name="descripcion")
    private String descipcion;
    @Column(name="fecha_de_inicio")
    private LocalDate fechaDeInicio;
    @Column(name="fecha_de_fin")
    private LocalDate fechaDeFin;

}
