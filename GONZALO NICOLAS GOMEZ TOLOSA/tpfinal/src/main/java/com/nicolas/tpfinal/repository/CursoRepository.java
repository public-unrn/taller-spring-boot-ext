package com.nicolas.tpfinal.repository;

import com.nicolas.tpfinal.domain.Curso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface CursoRepository extends JpaRepository<Curso,Long> {

    @Query("SELECT c FROM Curso c") // punto A consultas
    List<Curso> listarCursos();
        @Query("SELECT c FROM Curso c WHERE c.fechaDeInicio > DATE ('2020-02-01')")// punto F consultas
    List<Curso> listarCursosMayorA();


}
