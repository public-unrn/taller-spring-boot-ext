package com.nicolas.tpfinal.domain;

import lombok.ToString;


public enum Estado {
    ACEPTADA, RECHAZADA, PENDIENTE
}
