package com.nicolas.tpfinal;

import com.nicolas.tpfinal.domain.Curso;
import com.nicolas.tpfinal.domain.Estado;
import com.nicolas.tpfinal.domain.Estudiante;
import com.nicolas.tpfinal.domain.Inscripcion;
import com.nicolas.tpfinal.repository.CursoRepository;
import com.nicolas.tpfinal.repository.EstudianteRepository;
import com.nicolas.tpfinal.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageRequest;


import java.time.LocalDate;

@SpringBootApplication
public class TpfinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpfinalApplication.class, args);
	}
	@Autowired
	CursoRepository cursoRepository;
	@Autowired
	EstudianteRepository estudianteRepository;
	@Autowired
	InscripcionRepository inscripcionRepository;
	 @Bean
	public CommandLineRunner comandLineRunner(ApplicationContext ctx){
		 return arg->{
			 //tp de consultas

			 //punto A
			 cursoRepository.findAll();

			 //PUNTO B
			estudianteRepository.findAll();

			//PUNTO C
			estudianteRepository.buscarEstudiantesPorDniYApellido();

			//PUNTO D
			estudianteRepository.findByDniGreaterThanAndApellidoIgnoreCase(20000000,"romero");

			//PUNTO E
			inscripcionRepository.findByEstadoIn(Estado.PENDIENTE,Estado.ACEPTADA);

			//PUNTO F
			cursoRepository.listarCursosMayorA();

			//PUNTO G
			inscripcionRepository.inscriptosPorEstadoParametro(Estado.ACEPTADA);
			//PUNTO H
			inscripcionRepository.inscriptosPorEstado(Estado.RECHAZADA.toString());

			//PUNTO I
			 estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(1,5));
			 estudianteRepository.findAllByOrderByDniAsc(PageRequest.of(0,2));
		 };
	 }
}
