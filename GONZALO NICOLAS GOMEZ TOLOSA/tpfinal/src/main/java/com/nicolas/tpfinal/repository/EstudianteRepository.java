package com.nicolas.tpfinal.repository;

import com.nicolas.tpfinal.domain.Estudiante;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.web.PageableDefault;

import java.util.List;

public interface EstudianteRepository extends JpaRepository<Estudiante,Long> {
    @Query("SELECT e FROM Estudiante e") // punto B consultas
    List<Estudiante>ListarEstudiantes();

    @Query("SELECT e FROM Estudiante e WHERE e.dni>20000000 AND LOWER(e.apellido)='romero'")// punto C consultas
    List<Estudiante> buscarEstudiantesPorDniYApellido();

    //punto E consultas
    public List<Estudiante> findByDniGreaterThanAndApellidoIgnoreCase(int dni, String apellido);

    //punto I consultas
    Page<Estudiante>findAllByOrderByDniAsc(@PageableDefault Pageable pageable);
}
