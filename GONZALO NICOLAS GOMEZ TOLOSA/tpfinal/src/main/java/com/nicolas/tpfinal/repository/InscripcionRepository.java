package com.nicolas.tpfinal.repository;

import com.nicolas.tpfinal.domain.Estado;
import com.nicolas.tpfinal.domain.Inscripcion;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface InscripcionRepository extends JpaRepository<Inscripcion,Long> {
    @Query("SELECT i FROM Inscripcion i WHERE i.estado='PENDIENTE' OR i.estado='RECHAZADO'")//punto D consultas
    public List <Inscripcion> findByEstadoPendieteOrRechazado();

    //punto E consultas derivadas
    public List <Inscripcion> findByEstadoIn(Estado... estado);

    @Query(value="SELECT * FROM inscripciones i WHERE i.estado= :estado",nativeQuery = true)
    List<Inscripcion> inscriptosPorEstado(@Param("estado") String estado);
    @Query("SELECT i FROM Inscripcion i WHERE i.estado= :estado")
    List<Inscripcion> inscriptosPorEstadoParametro(@Param("estado") Estado estado);
}
