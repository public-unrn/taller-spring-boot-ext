package com.nicolas.tpfinal.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;
 @Data
 @AllArgsConstructor
public class EstudianteDto {
    private String nombre;

    private String apellido;

    private String email;

    private int dni;

    private LocalDate fechaDeNacimiento;
}
