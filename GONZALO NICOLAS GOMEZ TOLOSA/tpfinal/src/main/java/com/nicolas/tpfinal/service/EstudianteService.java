package com.nicolas.tpfinal.service;

import com.nicolas.tpfinal.domain.Estudiante;
import com.nicolas.tpfinal.dto.EstudianteDto;
import com.nicolas.tpfinal.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;
    public List<EstudianteDto> findAll(){
        return estudianteRepository.findAll()
                .stream()
                .map(e->new EstudianteDto(e.getNombre(),e.getApellido(),e.getEmail(),e.getDni(),e.getFechaDeNacimiento()))
                .collect(Collectors.toList());
    }
    public EstudianteDto find (Long id) {
   Estudiante e =estudianteRepository.findById(id).
           orElseThrow(()->new RuntimeException("El estudiante de id:"+id+" no existe"));
    return new EstudianteDto(e.getNombre(),e.getApellido(),e.getEmail(),e.getDni(),e.getFechaDeNacimiento());
    }

    public void create(EstudianteDto estudianteDto){
       estudianteRepository.save(new Estudiante(null,estudianteDto.getNombre(),estudianteDto.getApellido(),estudianteDto.getEmail(),estudianteDto.getDni(),estudianteDto.getFechaDeNacimiento()));
    }

    public void update(Long id,EstudianteDto estudianteDto){
        estudianteRepository.save(new Estudiante(id,estudianteDto.getNombre(),estudianteDto.getApellido(),estudianteDto.getEmail(),estudianteDto.getDni(),estudianteDto.getFechaDeNacimiento()));
    }

    public void delete(Long id){
        estudianteRepository.deleteById(id);
    }
}
