package com.nicolas.tpfinal.controller;

import com.nicolas.tpfinal.domain.Estado;
import com.nicolas.tpfinal.dto.InscripcionDto;
import com.nicolas.tpfinal.service.InscripcionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/inscripcion")
public class InscripcionController {
    @Autowired
    InscripcionService inscripcionService;
    @PostMapping
    public void crearInscripcion( @RequestParam("idCurso") Long idCurso,@RequestParam("idEstudiante") Long idEstudiante,@RequestBody Estado estado){
        inscripcionService.crearInscripcion(idCurso,idEstudiante, estado);
    }
}
