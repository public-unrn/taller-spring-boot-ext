package com.nicolas.tpfinal.domain;

import jakarta.persistence.*;
import lombok.*;


import java.time.LocalDate;
@Entity
@Table(name="estudiantes")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Estudiante {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name="nombre")
    private String nombre;
    @Column(name="apellido")
    private String apellido;
    @Column(name="email")
    private String email;
    @Column(name="dni")
    private int dni;
    @Column(name="fecha_de_nacimiento")
    private LocalDate fechaDeNacimiento;
}
