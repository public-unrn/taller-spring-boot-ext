package com.nicolas.tpfinal.controller;

import com.nicolas.tpfinal.domain.Curso;
import com.nicolas.tpfinal.dto.CursoDto;
import com.nicolas.tpfinal.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/cursos")
public class CursoController {
    @Autowired
    private CursoService cursoService;

    @GetMapping
    public List<CursoDto> findAll(){
        return cursoService.findAll();
    }
    @PostMapping()
    public void crearCurso(@RequestBody CursoDto cursoDto){
        cursoService.addCurso(cursoDto);
    }
}
