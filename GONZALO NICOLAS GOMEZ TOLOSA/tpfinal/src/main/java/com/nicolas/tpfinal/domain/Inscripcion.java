package com.nicolas.tpfinal.domain;

import jakarta.persistence.*;
import lombok.*;


import java.time.LocalDate;
@Entity
@Table(name="inscripciones")
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Inscripcion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name="cursos_id")
    private Curso curso;
    @ManyToOne
    @JoinColumn(name="estudiante_id")
    private Estudiante estudiante;
    @Column(name="fecha_de_inscripcion")
    private LocalDate fechaDeInscripcion;
    @Column(name="estado")
    @Enumerated(value=EnumType.STRING)
    private Estado estado;
}
