package com.nicolas.tpfinal.service;

import com.nicolas.tpfinal.domain.Curso;
import com.nicolas.tpfinal.dto.CursoDto;
import com.nicolas.tpfinal.repository.CursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CursoService {
@Autowired
private CursoRepository cursoRepository;
    public List<CursoDto>findAll(){
        return cursoRepository.findAll()
                .stream()
                .map(c->new CursoDto
                        (c.getNombre(),c.getDescipcion(),c.getFechaDeInicio(),c.getFechaDeFin()))
                .collect(Collectors.toList());
    }
    public void addCurso(CursoDto cursoDto){
        Curso curso = new Curso(null,cursoDto.getNombre(),cursoDto.getDescipcion(),cursoDto.getFechaDeInicio(),cursoDto.getFechaDeFin());
        cursoRepository.save(curso);
    }
}
