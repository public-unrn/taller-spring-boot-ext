package com.nicolas.tpfinal.service;

import com.nicolas.tpfinal.domain.Curso;
import com.nicolas.tpfinal.domain.Estado;
import com.nicolas.tpfinal.domain.Estudiante;
import com.nicolas.tpfinal.domain.Inscripcion;
import com.nicolas.tpfinal.repository.CursoRepository;
import com.nicolas.tpfinal.repository.EstudianteRepository;
import com.nicolas.tpfinal.repository.InscripcionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;

@Service
public class InscripcionService {
    @Autowired
    CursoRepository cursoRepository;
    @Autowired
    EstudianteRepository estudianteRepository;
    @Autowired
    InscripcionRepository inscripcionRepository;
    public void crearInscripcion(Long idCurso, Long idEstudiante, Estado estado){

        Curso curso = cursoRepository
                .findById(idCurso)
                .orElseThrow(()->new RuntimeException("El curso no existe"));

        Estudiante estudiante=estudianteRepository
        .findById(idEstudiante)
                .orElseThrow(()->new RuntimeException("El estudiante no existe"));

        int edad = Period.between(estudiante.getFechaDeNacimiento(), LocalDate.now()).getYears();
        if(edad<18){
            throw new RuntimeException("El estudiante debe ser mayor de edad");
        }
        Inscripcion inscripcion = new Inscripcion(null,curso,estudiante,LocalDate.now(),estado);
        inscripcionRepository.save(inscripcion);
    }
}
