package com.nicolas.tpfinal.dto;

import com.nicolas.tpfinal.domain.Curso;
import com.nicolas.tpfinal.domain.Estado;
import com.nicolas.tpfinal.domain.Estudiante;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class InscripcionDto {
    private CursoDto cursoDto;

    private EstudianteDto estudianteDto;

    private LocalDate fechaDeInscripcion;

    private Estado estado;
}
